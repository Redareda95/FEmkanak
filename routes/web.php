<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


 //front_end Home Page  & Contact Form
          Route::get('/', 'commonController@index');
          Route::get('/getrestricted', 'Dashboard\AvailableController@getRestricted');
          Route::get('/verfiy/{token}', 'commonController@verfiy');
          
          Route::post('/send_code', 'FrontController@send_code');
          Route::post('/verfiy_phone', 'FrontController@verfiy_phone');
          
          Route::get('/resend', 'commonController@resend');
          Route::get('/getDisabledDays', 'Dashboard\AvailableController@getDisabledDays');
          Route::get('/get_notification', 'commonController@get_notification');
          
          Route::post("/send_message",'commonController@send_message');

 //Front - End Contact Form 
 		  Route::get('/book_now', 'FrontController@bookNow');	         
 //Front - End Edit Profile
 		  Route::get('/edit_profile', 'FrontController@edit_profile');
 		  Route::post('/edit_profile_action', 'FrontController@edit_profile_action');
 //Front - End Booking Form 
 		  Route::get('/book_now', 'FrontController@bookNow');
 		  Route::post('/order_now', 'FrontController@SaveOrder');         
 		  Route::get('/order_now', 'commonController@index');         
 		  Route::get('/service/{id}', 'FrontController@servicePrice');
 //Previous Requests
          Route::get('/requests', 'FrontController@Requests');

//Admin panel
Route::group(['middleware'=>'roles', 'roles'=>'admin'], function(){

	//Roles
	  Route::get('/admin', 'Dashboard\RolesController@showPanel');
	  Route::get('/users', 'Dashboard\RolesController@showUsers');
	  Route::post('/add-roles', 'Dashboard\RolesController@addRole');

	 //Users
	  Route::get('/admin/all-users', 'Dashboard\UsersController@index');
	  Route::get('/admin/user_edit/{id}', 'Dashboard\UsersController@edit');
	  Route::get('/admin/users/history/{id}', 'Dashboard\UsersController@history');
	   Route::get('/admin/users/emp_history/{id}', 'Dashboard\EmployeeController@emp_history');
	  
	  Route::patch('/admin/user_update/{id}', 'Dashboard\UsersController@update');

	//site info
	  Route::get('/admin/site_settings', 'Dashboard\SiteSettingController@index');
	  Route::get('/admin/edit_info/{id}', 'Dashboard\SiteSettingController@edit');
	  Route::patch('/admin/updateSiteInfo/{id}', 'Dashboard\SiteSettingController@update');

	//Services
	  Route::get('/admin/service', 'Dashboard\ServiceController@index');
	  Route::get('/admin/service_edit/{id}', 'Dashboard\ServiceController@edit');
	  Route::get('/admin/service_create', 'Dashboard\ServiceController@create');
	  Route::post('/admin/service_store', 'Dashboard\ServiceController@store');
	  Route::patch('/admin/update_service/{id}', 'Dashboard\ServiceController@update');

	//Cars
	  Route::get('/admin/car', 'Dashboard\CarController@index');
	  Route::get('/admin/car/history/{id}', 'Dashboard\CarController@history');
	  Route::get('/admin/car_edit/{id}', 'Dashboard\CarController@edit');
	  Route::get('/admin/car_create', 'Dashboard\CarController@create');
	  Route::post('/admin/car_store', 'Dashboard\CarController@store');
	  Route::patch('/admin/car_update/{id}', 'Dashboard\CarController@update');

	//Employee
	  Route::get('/admin/employee', 'Dashboard\EmployeeController@index');
	  Route::get('/admin/employee_edit/{id}', 'Dashboard\EmployeeController@edit');
	  Route::get('/admin/employee_create', 'Dashboard\EmployeeController@create');
	  Route::post('/admin/employee_store', 'Dashboard\EmployeeController@store');
	  Route::patch('/admin/employee_update/{id}', 'Dashboard\EmployeeController@update');
          
    //orders_info  
	  Route::get('/admin/orders', 'Dashboard\orderController@index');
	  Route::get('/admin/inprogress_orders', 'Dashboard\orderController@inprogress_orders');
	  Route::get('/admin/rejected_orders', 'Dashboard\orderController@rejected_orders');
	  Route::get('/admin/done_orders', 'Dashboard\orderController@done_orders');
	  Route::get('/admin/pending_orders', 'Dashboard\orderController@pending_orders');
	  Route::post('/admin/store_orders', 'Dashboard\orderController@store');
	  Route::get('/admin/edit_orders/{id}', 'Dashboard\orderController@edit');
	  Route::post('/admin/update_orders/{id}', 'Dashboard\orderController@update');
	  Route::get('/admin/assign_order/{id}', 'Dashboard\orderController@assign_order');
      Route::post('/admin/select_status', 'Dashboard\orderController@update_status');
	  Route::post('/admin/assign_order_action/{id}', 'Dashboard\orderController@assign_order_action');
	  Route::get('/admin/add_orders', 'Dashboard\orderController@create');

	//Available Days
	  Route::get('/admin/available_days', 'Dashboard\AvailableController@index');
	  Route::patch('/admin/available_day_edit/{id}', 'Dashboard\AvailableController@update');
	  Route::get('/admin/restrict_day', 'Dashboard\AvailableController@add_day');
	  Route::post('/admin/restrict_day', 'Dashboard\AvailableController@store');
	  Route::get('/admin/restrict/{date}', 'Dashboard\AvailableController@ajaxTime');

	//public_texts 
	  Route::get('/admin/public_texts', 'Dashboard\public_textsController@index');
	  Route::get('/admin/public_texts_edit/{id}', 'Dashboard\public_textsController@edit');
	  Route::post('/admin/public_texts_update/{id}', 'Dashboard\public_textsController@update');
	  
          
	 //Messages
	  Route::get('/admin/mails', 'Dashboard\ContactFormController@index');
	  Route::get('/admin/mail/{id}', 'Dashboard\ContactFormController@show');
	 //Add Admin
	  Route::get('/admin/admins', 'Dashboard\UsersController@admins');
	  Route::get('/admin/register', 'Dashboard\UsersController@register');
	  Route::post('/admin/register', 'Dashboard\UsersController@addAdmin');
         
	  

});

//Editor panel
Route::group(['middleware'=>'roles', 'roles'=>['admin', 'editor']], function(){
	  Route::get('/admin', 'Dashboard\RolesController@showPanel');

    //orders_info  
	  Route::get('/admin/orders', 'Dashboard\orderController@index');
	  Route::get('/admin/inprogress_orders', 'Dashboard\orderController@inprogress_orders');
	  Route::get('/admin/rejected_orders', 'Dashboard\orderController@rejected_orders');
	  Route::get('/admin/done_orders', 'Dashboard\orderController@done_orders');
	  Route::get('/admin/pending_orders', 'Dashboard\orderController@pending_orders');
	  Route::post('/admin/store_orders', 'Dashboard\orderController@store');
	  Route::get('/admin/edit_orders/{id}', 'Dashboard\orderController@edit');
	  Route::post('/admin/update_orders/{id}', 'Dashboard\orderController@update');
	  Route::get('/admin/assign_order/{id}', 'Dashboard\orderController@assign_order');
      Route::post('/admin/select_status', 'Dashboard\orderController@update_status');
	  Route::post('/admin/assign_order_action/{id}', 'Dashboard\orderController@assign_order_action');
});
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::post('/admin/delete', 'commonController@delete');
