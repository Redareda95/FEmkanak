-- phpMyAdmin SQL Dump
-- version 4.7.7
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Aug 26, 2018 at 03:10 AM
-- Server version: 5.6.39-83.1
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `gtsaw1_quicks`
--

-- --------------------------------------------------------

--
-- Table structure for table `assigned_order`
--

CREATE TABLE `assigned_order` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `order_id` int(10) UNSIGNED NOT NULL,
  `car_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `available_days`
--

CREATE TABLE `available_days` (
  `id` int(10) UNSIGNED NOT NULL,
  `from` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `to` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `available_days`
--

INSERT INTO `available_days` (`id`, `from`, `to`, `created_at`, `updated_at`) VALUES
(1, '12:00', '10:00', '2018-07-26 13:16:29', '2018-08-10 22:46:13'),
(2, '10:00', '11:00', '2018-07-26 13:16:29', '2018-07-26 13:16:29'),
(3, '11:00', '12:00', '2018-07-26 13:16:29', '2018-07-26 13:16:29'),
(4, '12:00', '1:00', '2018-07-26 13:16:29', '2018-07-26 13:16:29'),
(5, '1:00', '2:00', '2018-07-26 13:16:29', '2018-07-26 13:16:29'),
(6, '2:00', '3:00', '2018-07-26 13:16:29', '2018-07-26 13:16:29'),
(7, '3:00', '4:00', '2018-07-26 13:16:29', '2018-07-26 13:16:29'),
(8, '4:00', '5:00', '2018-07-26 13:16:29', '2018-07-26 13:16:29');

-- --------------------------------------------------------

--
-- Table structure for table `cars`
--

CREATE TABLE `cars` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `desc` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `cars`
--

INSERT INTO `cars` (`id`, `name`, `desc`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Volvo', '<p>3am reda</p>', '1', '2018-07-21 13:33:43', '2018-08-08 02:30:45'),
(2, 'BAIC', '<p>Muhammad Samy</p>', '1', '2018-08-05 20:37:35', '2018-08-08 02:30:28');

-- --------------------------------------------------------

--
-- Table structure for table `contact_forms`
--

CREATE TABLE `contact_forms` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `subject` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `message` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `read` enum('read','un_read') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'un_read',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `contact_forms`
--

INSERT INTO `contact_forms` (`id`, `name`, `email`, `subject`, `message`, `read`, `created_at`, `updated_at`) VALUES
(2, 'dfsaf', 'dsafsfs@najklsd.com', 'skdjfklsaf', 'msaklfmk,', 'read', '2018-07-28 13:57:14', '2018-08-20 02:47:51'),
(3, 'abubakr testing', 'abubakr.sokarno@gmail.com', 'testing', 'there are some issues I noticed and we need to talk xD', 'read', '2018-07-28 21:15:56', '2018-07-28 22:26:34'),
(4, 'Ahmed Reda', 'reda@example.com', 'hii', 'jdjfdjjfdf', 'read', '2018-07-30 21:28:28', '2018-07-30 21:38:18'),
(5, 'قذذذذذ', 'Vvvvv@ffg.com', 'Fgjjhvcxd ox xxdfhvc', 'Jgvgccxcccvnkkkkhf', 'read', '2018-08-02 02:35:14', '2018-08-06 00:06:38');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2018_07_21_082944_create_roles_table', 1),
(4, '2018_07_21_085805_create_user_role_table', 1),
(5, '2018_07_21_105733_create_site_settings_table', 2),
(8, '2018_07_21_133646_create_services_table', 3),
(9, '2018_07_21_133659_create_cars_table', 3),
(10, '2018_07_21_133711_create_orders_table', 3),
(11, '2018_07_21_133734_create_assigned_order_table', 3),
(12, '2018_07_22_090623_create_contact_forms_table', 4),
(13, '2018_07_23_093024_default_val_staus', 5),
(14, '2018_07_25_101542_add_image_to_users', 6),
(15, '2018_07_25_124605_public_texts', 6),
(16, '2018_07_26_124052_create_available_days_table', 7),
(17, '2018_07_26_124135_create_restricted_days_table', 7);

-- --------------------------------------------------------

--
-- Table structure for table `notification`
--

CREATE TABLE `notification` (
  `id` int(10) UNSIGNED NOT NULL,
  `link` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `order_text` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `order_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `seen` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `notification`
--

INSERT INTO `notification` (`id`, `link`, `order_text`, `order_id`, `seen`) VALUES
(1, 'http://testfymkanak.hatgwez.com/admin/edit_orders/14', 'new order created at 2018-07-30 7', '14', '1'),
(2, 'http://testfymkanak.hatgwez.com/admin/edit_orders/15', 'new order created at 2018-07-30 5', '15', '1'),
(3, 'http://testfymkanak.hatgwez.com/admin/edit_orders/16', 'new order created at 2018-07-30 8', '16', '1'),
(4, 'http://testfymkanak.hatgwez.com/admin/edit_orders/17', 'new order created at 2018-07-31 4', '17', '1'),
(5, 'http://testfymkanak.hatgwez.com/admin/edit_orders/18', 'new order created at 2018-07-31 5', '18', '1'),
(6, 'http://testfymkanak.hatgwez.com/admin/edit_orders/19', 'New Order created at 2018-07-30 15:10:22 ', '19', '1'),
(7, 'http://testfymkanak.hatgwez.com/admin/edit_orders/20', 'New Order created at 2018-07-30 16:57:52 ', '20', '1'),
(8, 'http://testfymkanak.hatgwez.com/admin/edit_orders/21', 'New Order created at 2018-07-30 18:28:58 ', '21', '1'),
(9, 'http://testfymkanak.hatgwez.com/admin/edit_orders/22', 'New Order created at 2018-08-01 09:07:40 ', '22', '1'),
(10, 'http://testfymkanak.hatgwez.com/admin/edit_orders/23', 'New Order created at 2018-08-05 18:31:03 ', '23', '1'),
(11, 'http://testfymkanak.hatgwez.com/admin/edit_orders/24', 'New Order created at 2018-08-05 18:31:35 ', '24', '1'),
(12, 'http://testfymkanak.hatgwez.com/admin/edit_orders/26', 'New Order created at 2018-08-09 10:06:40 ', '26', '1'),
(13, 'http://testfymkanak.hatgwez.com/admin/edit_orders/27', 'New Order created at 2018-08-10 16:01:34 ', '27', '1'),
(14, 'http://testfymkanak.hatgwez.com/admin/edit_orders/28', 'New Order created at 2018-08-10 17:16:30 ', '28', '1'),
(15, 'http://testfymkanak.hatgwez.com/admin/edit_orders/29', 'New Order created at 2018-08-10 19:59:55 ', '29', '1');

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `service_id` int(10) UNSIGNED NOT NULL,
  `date` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `time` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `long` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `lat` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Pending',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`id`, `user_id`, `service_id`, `date`, `time`, `long`, `lat`, `status`, `created_at`, `updated_at`) VALUES
(28, 25, 1, '2018-08-16', '7', '31.432974004785137', '30.00058414341674', 'Pending', '2018-08-10 22:16:30', '2018-08-10 22:16:30'),
(29, 27, 1, '2018-08-23', '1', '31.43507775663852', '30.007998711444678', 'In progress', '2018-08-11 00:59:55', '2018-08-20 02:16:38');

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `public_texts`
--

CREATE TABLE `public_texts` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `public_texts`
--

INSERT INTO `public_texts` (`id`, `name`, `value`) VALUES
(1, 'Working Hours', '6 AM To 8 PM SUNDAY CLOSED[EN] 6 صباحا الى 8 مسائا الاحد اجازة'),
(2, 'Phone', '+1-202-555-0137[EN]+1-202-555-0137'),
(3, 'ADDRESS', '514 S. Magnolia St.Orlando , US[EN]514 S. Magnolia St.Orlando , US'),
(4, 'E - MAIL', 'dummymail@mail.com[EN]dummymail@mail.com'),
(5, 'CALL US NOW', '(+1)202-202-012[EN](+1)202-202-012');

-- --------------------------------------------------------

--
-- Table structure for table `restricted_days`
--

CREATE TABLE `restricted_days` (
  `id` int(10) UNSIGNED NOT NULL,
  `day` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `available_day_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `restricted_days`
--

INSERT INTO `restricted_days` (`id`, `day`, `available_day_id`, `created_at`, `updated_at`) VALUES
(6, '2018-07-30', 1, '2018-07-28 13:23:01', '2018-07-28 13:23:01'),
(7, '2018-07-30', 4, '2018-07-28 13:24:01', '2018-07-28 13:24:01'),
(8, '2018-07-30', 6, '2018-07-28 15:56:39', '2018-07-28 15:56:39'),
(9, '2018-07-30', 2, '2018-07-28 15:58:14', '2018-07-28 15:58:14'),
(10, '2018-07-31', 3, '2018-07-28 16:04:42', '2018-07-28 16:04:42'),
(11, '2018-07-30', 7, '2018-07-28 20:21:02', '2018-07-28 20:21:02'),
(12, '2018-07-30', 5, '2018-07-28 20:23:30', '2018-07-28 20:23:30'),
(13, '2018-07-30', 8, '2018-07-28 21:48:51', '2018-07-28 21:48:51'),
(14, '2018-07-31', 4, '2018-07-30 19:59:34', '2018-07-30 19:59:34'),
(15, '2018-07-31', 5, '2018-07-30 20:00:47', '2018-07-30 20:00:47'),
(16, '2018-07-31', 6, '2018-07-30 20:10:22', '2018-07-30 20:10:22'),
(17, '2018-08-03', 2, '2018-07-30 21:36:30', '2018-07-30 21:36:30'),
(18, '2018-07-31', 7, '2018-07-30 21:57:53', '2018-07-30 21:57:53'),
(19, '2018-07-31', 1, '2018-07-30 23:28:58', '2018-07-30 23:28:58'),
(20, '2018-08-02', 6, '2018-08-01 14:07:40', '2018-08-01 14:07:40'),
(21, '2018-08-07', 2, '2018-08-05 23:15:55', '2018-08-05 23:15:55'),
(22, '2018-08-20', 3, '2018-08-20 02:39:55', '2018-08-20 02:39:55');

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `desc` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `name`, `desc`, `created_at`, `updated_at`) VALUES
(1, 'admin', 'Has all normal user functionality & permissions to create, read, update, delete any information regarding the site preview.\r\nand also has the ability to assign orders to employee & cars.\r\ncan cancel and change order status.\r\nEventually can read site\'s Statistics.', '2018-07-21 09:42:17', NULL),
(2, 'editor', 'Has all Normal User\'s functionalities.\r\nCan assign orders to Employee\'s & Cars.', '2018-07-16 22:00:00', NULL),
(3, 'user', 'Has only Access to front-end site, starting from making an order to editing his profile & viewing previous requests.', '2018-07-21 09:42:35', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `services`
--

CREATE TABLE `services` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `desc` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `price` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `services`
--

INSERT INTO `services` (`id`, `name`, `desc`, `created_at`, `updated_at`, `price`) VALUES
(1, 'Car Wash', '<p>A car wash or auto wash is a facility used to clean the exterior and, in some cases, the interior of motor vehicles. Car washes can be self-serve, fully automated, or full-service with attendants who wash the vehicle.</p>', '2018-07-21 13:22:36', '2018-08-05 20:06:12', '200 Egp'),
(2, 'Car Care', '<p>- Checmical internal Wash&nbsp;</p>', '2018-08-05 20:02:14', '2018-08-05 20:26:37', '500 Egp');

-- --------------------------------------------------------

--
-- Table structure for table `site_settings`
--

CREATE TABLE `site_settings` (
  `id` int(10) UNSIGNED NOT NULL,
  `keywords` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `author` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `site_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `video_link` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `logo` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `favicon` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `about_company` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `fb_link` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `twitter_link` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `google_plus` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `youtube_link` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `linkedin_link` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `instagram_link` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `about_company_ar` longtext COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `site_settings`
--

INSERT INTO `site_settings` (`id`, `keywords`, `author`, `description`, `site_name`, `video_link`, `logo`, `favicon`, `about_company`, `fb_link`, `twitter_link`, `google_plus`, `youtube_link`, `linkedin_link`, `instagram_link`, `created_at`, `updated_at`, `about_company_ar`) VALUES
(1, 'dgfd,f dfdf,fdfdf,fdfdf,dfdfd,dfdf', 'gtsaw', '<p>Drive with Fymkanak and earn great money as an independent contractor. Get paid weekly just for helping our community of riders get rides around town. Be your own boss and get paid in fares for driving on your own schedule.</p>', 'FEmkanak', 'gfdsgfsgf', '1532628568.png', '1532628359.png', '<p>Drive with Fymkanak and earn great money as an independent contractor. Get paid weekly just for helping our community of riders get rides around town. Be your own boss and get paid in fares for driving on your own schedule.</p>', 'ghdgdgtrfg', 'fgfgf', 'gffgfg', 'fgfgfg', 'gsrtrg', 'fgfgdsg', NULL, '2018-08-05 23:59:04', '<p> يمكنك القيادة باستخدام Fymkanak وكسب أموال طائلة كمقاول مستقل. الحصول على دفع أسبوعي فقط لمساعدة مجتمعنا من الركائز ركوب الخيل في جميع أنحاء المدينة. كن مديرك الخاص وتحقق لك الأجر مقابل القيادة وفقًا لجدولك الخاص. </ p>');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `first_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_type` enum('admin','user','employee') COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `verify` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `first_name`, `last_name`, `email`, `image`, `password`, `phone`, `user_type`, `remember_token`, `verify`, `token`, `created_at`, `updated_at`) VALUES
(1, 'Ahmed', 'Reda', 'reda19952013@gmail.com', 'uploads/1533910024.jpeg', '$2y$10$MQxGlCSeWIPBv85FMTPSYebi4uUI10QHv0MkR1uIj/fpeRtYvxXMO', '01117745', 'admin', 'sIPEQLrxVAKkoXB2M17EunbvgB0lkCuUGMkofU4ObumlsIY6jai9EW5rGtTJ', '1', '', '2018-07-21 07:46:28', '2018-08-26 13:07:29'),
(25, 'myaw', 'otta', 'reda19952018@gmail.com', '', '$2y$10$x.IF6wdT5Bx7NyZi9Qzt2echccTt526Ded/d8u0ivugEys6iDSAd.', '01114388500', 'user', 'JuIJnhzN5PEDZ0EQgLSFmyzkyEHt5dIT1HgwSKg20bagwKzSOLFWqFamQRBl', '1', '728e9a37e048820294063b7bfd602d20', '2018-08-10 21:25:27', '2018-08-14 15:28:00'),
(26, 'ahmed', 'gouda', 'ahmedmoud@ymail.com', '', '$2y$10$eePjgWRaWM9msfMPcGJ5/OjGu/C8TMwe.1LrkP1dumbApVySDsHMS', '01117745889', 'user', 'quNpgqNgfOsAkKPWS7uLBE5ogpENjcw4KOPsM6MT53QyICOzhoax5Jd2MUCW', '1', 'f5de21b682992116de75630304cc272a', '2018-08-10 23:26:47', '2018-08-12 16:02:39'),
(27, 'Ola', 'Ahmed', 'Olaahmed262013@gmail.com', '', '$2y$10$mHgsjimpzQakro9G5skwAOVWKaF0V.mo.LsWbfKaUmlCZkRvT7U4G', '01280118028', 'user', NULL, '1', 'd2a7cb341e76f2ef6c112c074eab66fa', '2018-08-11 00:57:29', '2018-08-11 00:58:10');

-- --------------------------------------------------------

--
-- Table structure for table `user_role`
--

CREATE TABLE `user_role` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(11) NOT NULL,
  `role_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `user_role`
--

INSERT INTO `user_role` (`id`, `user_id`, `role_id`, `created_at`, `updated_at`) VALUES
(13, 9, 3, NULL, NULL),
(14, 10, 3, NULL, NULL),
(15, 11, 3, NULL, NULL),
(16, 12, 3, NULL, NULL),
(17, 13, 3, NULL, NULL),
(22, 1, 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `verify_phone`
--

CREATE TABLE `verify_phone` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED DEFAULT NULL,
  `phone` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `pin_code` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `verify` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `verify_phone`
--

INSERT INTO `verify_phone` (`id`, `user_id`, `phone`, `pin_code`, `verify`, `created_at`, `updated_at`) VALUES
(10, 25, '01114388500', '7914', '1', '2018-08-10 21:25:27', '2018-08-10 22:16:12'),
(11, 26, '01117745889', '8589', '1', '2018-08-10 23:26:47', '2018-08-10 23:38:57'),
(12, 27, '01280118028', '6281', '1', '2018-08-11 00:57:29', '2018-08-11 00:58:36');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `assigned_order`
--
ALTER TABLE `assigned_order`
  ADD PRIMARY KEY (`id`),
  ADD KEY `assigned_order_user_id_index` (`user_id`),
  ADD KEY `assigned_order_order_id_index` (`order_id`),
  ADD KEY `assigned_order_car_id_index` (`car_id`);

--
-- Indexes for table `available_days`
--
ALTER TABLE `available_days`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cars`
--
ALTER TABLE `cars`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `contact_forms`
--
ALTER TABLE `contact_forms`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `notification`
--
ALTER TABLE `notification`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`),
  ADD KEY `orders_user_id_index` (`user_id`),
  ADD KEY `orders_service_id_index` (`service_id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `public_texts`
--
ALTER TABLE `public_texts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `restricted_days`
--
ALTER TABLE `restricted_days`
  ADD PRIMARY KEY (`id`),
  ADD KEY `restricted_days_available_day_id_index` (`available_day_id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `services`
--
ALTER TABLE `services`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `site_settings`
--
ALTER TABLE `site_settings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- Indexes for table `user_role`
--
ALTER TABLE `user_role`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `verify_phone`
--
ALTER TABLE `verify_phone`
  ADD PRIMARY KEY (`id`),
  ADD KEY `verify_phone_user_id_index` (`user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `assigned_order`
--
ALTER TABLE `assigned_order`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `available_days`
--
ALTER TABLE `available_days`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `cars`
--
ALTER TABLE `cars`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `contact_forms`
--
ALTER TABLE `contact_forms`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `notification`
--
ALTER TABLE `notification`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;

--
-- AUTO_INCREMENT for table `public_texts`
--
ALTER TABLE `public_texts`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `restricted_days`
--
ALTER TABLE `restricted_days`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `services`
--
ALTER TABLE `services`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `site_settings`
--
ALTER TABLE `site_settings`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;

--
-- AUTO_INCREMENT for table `user_role`
--
ALTER TABLE `user_role`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;

--
-- AUTO_INCREMENT for table `verify_phone`
--
ALTER TABLE `verify_phone`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `assigned_order`
--
ALTER TABLE `assigned_order`
  ADD CONSTRAINT `assigned_order_car_id_foreign` FOREIGN KEY (`car_id`) REFERENCES `cars` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `assigned_order_order_id_foreign` FOREIGN KEY (`order_id`) REFERENCES `orders` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `assigned_order_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `orders`
--
ALTER TABLE `orders`
  ADD CONSTRAINT `orders_service_id_foreign` FOREIGN KEY (`service_id`) REFERENCES `services` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `orders_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `restricted_days`
--
ALTER TABLE `restricted_days`
  ADD CONSTRAINT `restricted_days_available_day_id_foreign` FOREIGN KEY (`available_day_id`) REFERENCES `available_days` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
