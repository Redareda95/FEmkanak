<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class verify_phone extends Model
{
    protected $table = "verify_phone";
    public $timestamps = true;
}
