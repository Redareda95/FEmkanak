<?php

namespace App\Http\Controllers\Dashboard;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Car;

class CarController extends Controller
{
    public function index()
    {
    	$cars = Car::all();

    	return view('dashboard.cars.cars', compact('cars'));
    }

    public function edit($id)
    {
    	$car = Car::find($id);

    	return view('dashboard.cars.edit', compact('car'));
    }

    public function create()
    {
    	return view('dashboard.cars.add');
    }

    public function update(Request $request, $id) 
    {
    	$this->validate(request(),[
            'name'=>'required',
            'desc'=>'required',
        ]);

        Car::where('id', $id)->update($request->except(['_token', '_method']));

        session()->flash('message', 'Car Is Updated');

        return redirect('/admin/car');

    }

    public function store(Request $request) 
    {
    	$this->validate(request(),[
            'name'=>'required',
            'desc'=>'required',
        ]);

        $car = new Car;
        $car->name = $request->name;
        $car->desc = $request->desc;
        $car->save();

        session()->flash('message', 'Car Is Created');

        return redirect('/admin/car');

    }
    public function history($id){
        $assign_orders = \App\assign_order::where('car_id',$id)->get();
        $array_of_orders = array();
        $car = Car::find($id);
        $count_rej_order = 0; $count_Inprogress_order = 0;
        $count_Done_order = 0; $count_pending_order = 0;
        foreach($assign_orders as $assigned_order){
            $order = \App\Order::find($assigned_order->order_id);
            $service = \App\Service::find($order->service_id);
            $order->service_name = $service->name;
            array_push($array_of_orders,$order);
            if($order->status =="Done"){$count_Done_order++;}
            if($order->status =="Rejected"){$count_rej_order++;}
            if($order->status =="Pending"){$count_pending_order++;}
            if($order->status =="In progress"){$count_Inprogress_order++;}
        }
        return view("dashboard.cars.car_history",compact('array_of_orders','car','count_rej_order','count_Done_order','count_Inprogress_order','count_pending_order'));
    }
}
