<?php

namespace App\Http\Controllers\Dashboard;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\Order;
use DB;
use Illuminate\Support\Facades\Hash;


class UsersController extends Controller
{
    public function index()
    {
    	$users = User::where('user_type', 'user')->get();

    	return view('dashboard.users.users', compact('users'));
    }

    public function edit($id)
    {
    	$user = User::find($id);

    	return view('dashboard.users.edit', compact('user'));
    }

    public function update(Request $request, $id) 
    {
    	$this->validate(request(),[
            'first_name' => 'required',
            'last_name' => 'required',
            'phone' => 'required|unique:users,phone,'.$id,
            'email'=>'required|email|unique:users,email,'.$id,
        ]);

        User::where('id', $id)->update($request->except(['_token', '_method']));

        session()->flash('message', 'User Is Updated');

        return redirect('/admin/all-users');

    }

    public function register()
    {
        return view('dashboard.admins.add');
    }

    public function addAdmin(Request $request)
    {
        $this->validate(request(),[
            'first_name' => 'required',
            'last_name' => 'required',
            'phone' => 'required|unique:users',
            'email'=>'required|email|unique:users',
            'password'=>'required|string|min:6|confirmed'
        ]);

        $pass = $request->password;

        $admin = new User;
        $admin->first_name = $request->first_name;
        $admin->last_name = $request->last_name;
        $admin->phone = $request->phone;
        $admin->email = $request->email;
        $admin->user_type = 'admin';
        $admin->password = Hash::make($pass);
        $admin->save();

        $role = DB::table('user_role')->insert(
                                                ['user_id' => $admin->id, 'role_id' => 1]
                                            );
        return redirect('/admin');
    }

    public function admins()
    {
        $admins = User::where('user_type', 'admin')->get();

        return view('dashboard.admins.admins', compact('admins'));
    }
    
    public function history($id){
        $history = Order::where('user_id',$id)->orderBy('created_at','asc')->get();
        foreach($history as $order){
            $service = \App\Service::find($order->service_id);
            $order->service_name = $service->name;
        }
        $user = \App\User::find($id);
        $count_rej_order = Order::where('user_id',$id)->where('status','rejected')->count();
        
        $count_Done_order = Order::where('user_id',$id)->where('status','Done')->count();
        
        $count_pending_order = Order::where('user_id',$id)->where('status','Pending')->count();
        
        $count_Inprogress_order = Order::where('user_id',$id)->where('status','In progress')->count();
        return view('dashboard.users.user_history',compact('history','user','count_rej_order','count_Done_order','count_pending_order','count_Inprogress_order'));
    }
}
