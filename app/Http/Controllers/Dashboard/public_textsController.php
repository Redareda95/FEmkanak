<?php

namespace App\Http\Controllers\Dashboard;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\public_texts;
class public_textsController extends Controller
{
    public function index(){
        $public_texts = public_texts::get();
        return view('dashboard.public_text.public_texts', compact('public_texts'));
    }
    public function edit($id){
        $public_text = public_texts::find($id);
        return view('dashboard.public_text.edit_public_texts', compact('public_text'));
        
    }
    public function update($id,Request $request){
        $request->validate([
            'value'=>'required',
        ]);
        $value = $request->input('value');
        $arabicvalue = $request->input('arabicvalue');
        $public_text= public_texts::find($id);
        $public_text->value=$value.'[EN]'.$arabicvalue;
        $public_text->save();
        session()->flash('message', 'text updated successfully');

        return redirect('admin/public_texts');
        
    }
}
