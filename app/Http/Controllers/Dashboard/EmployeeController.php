<?php

namespace App\Http\Controllers\Dashboard;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use App\User;

class EmployeeController extends Controller
{
    public function index()
    {
    	$employees = User::where('user_type', 'employee')->get();

    	return view('dashboard.employees.employees', compact('employees'));
    }

    public function edit($id)
    {
    	$employee = User::find($id);

    	return view('dashboard.employees.edit', compact('employee'));
    }

    public function create()
    {
    	return view('dashboard.employees.add');
    }

    public function update(Request $request, $id) 
    {
    	$this->validate(request(),[
            'first_name' => 'required',
            'last_name' => 'required',
            'phone' => 'required|unique:users,phone,'.$id,
            'email'=>'required|email|unique:users,email,'.$id,
        ]);

        User::where('id', $id)->update($request->except(['_token', '_method']));

        session()->flash('message', 'Employee Is Updated');

        return redirect('/admin/employee');

    }

    public function store(Request $request) 
    {
    	$this->validate(request(),[
           'first_name' => 'required',
            'last_name' => 'required',
            'phone' => 'required|unique:users',
            'email'=>'required|email|unique:users',
        ]);

        $employee = new User;
        $employee->first_name = $request->first_name;
        $employee->last_name = $request->last_name;
        $employee->phone = $request->phone;
        $employee->email = $request->email;
        $employee->user_type = 'employee';
        $employee->password = Hash::make('123456');
        $employee->save();

        session()->flash('message', 'Employee Is Created');

        return redirect('/admin/employee');

    }
    public function emp_history($id){
        $assign_orders = \App\assign_order::where('user_id',$id)->get();
        $array_of_orders = array();
        $employee = \App\User::find($id);
        $count_rej_order = 0; $count_Inprogress_order = 0;
        $count_Done_order = 0; $count_pending_order = 0;
        foreach($assign_orders as $assigned_order){
            $order = \App\Order::find($assigned_order->order_id);
            $service = \App\Service::find($order->service_id);
            $car = \App\Car::find($assigned_order->car_id);
            $order->service_name = $service->name;
            array_push($array_of_orders,$order);
            if($order->status =="Done"){$count_Done_order++;}
            if($order->status =="Rejected"){$count_rej_order++;}
            if($order->status =="Pending"){$count_pending_order++;}
            if($order->status =="In progress"){$count_Inprogress_order++;}
        }
        return view("dashboard.employees.emp_history",compact('array_of_orders','employee','count_rej_order','count_Done_order','count_Inprogress_order','count_pending_order'));
    
    }
}
