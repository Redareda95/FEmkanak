<?php

namespace App\Http\Controllers\Dashboard;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\AvailableDay;
use App\RestrictedDay;
use DB;
use App\Order;
use App\Car;

class AvailableController extends Controller
{
	public function index()
	{
		$days = AvailableDay::all();

		return view('dashboard.days.days', compact('days'));
	}    

	public function update(Request $request, $id)
	{
		$this->validate(request(),[
	           'from'=>'required',
	           'to'=>'required',
	       ]);

		$from = $request->from;

		$to = $request->to;

		AvailableDay::where('id', $id)->update(['from' => $from, 'to' => $to]);

		session()->flash('message', 'Day Is Updated');

		return redirect('/admin/available_days');
	}	

	public function add_day()
	{
    	$date =   date('Y-m-d', strtotime("+1 day"));

		return view('dashboard.days.add_restricted',compact('date'));
	}

	public function store(Request $request)
	{
		$this->validate(request(),[
				'day'=>'required',
				'available_day_id'=>'required'
		]);

		$restrict = new RestrictedDay;
		$restrict->day = $request->day;
		$restrict->available_day_id = $request->available_day_id;
		$restrict->save();

		session()->flash('message', 'This timing is restricted');

		return redirect('/admin/available_days');

	}

	public function ajaxTime($date)
	{	
		$restricted = RestrictedDay::where('day',$date)->get(['available_day_id']);
		$days_without_restricted = DB::table('available_days')->whereNotIn('id',$restricted)->get();

		$data = '';
        foreach($days_without_restricted as $day)
        {
            $data .= '<option value="' . $day->id . '">' . $day->from .' </option>';
        }
        return response()->Json(['message' => $data]); 
	}
        public function getRestricted(Request $request){
            $date = $request->input('date');
            $restricted = RestrictedDay::where('day',$date)->get(['available_day_id']);
	    $days_without_restricted = DB::table('available_days')->whereNotIn('id',$restricted)->get();
	$data = '';
        foreach($days_without_restricted as $day)
        {
            $data .= '<option value="' . $day->id . '">' . $day->from .' </option>';
        }
        return response()->Json(['message' => $data]); 
	
        }
      public function  getDisabledDays(){
          $orders = Order::get();
          $cars = Car::count();
          $hours = AvailableDay::count();
          $max_orders = $cars * $hours ;
          $array = array();
          foreach ($orders  as  $order){
              
              $count_order = Order::where('date',$order->date)->where('status','In progress')->orWhere('status','Pending')->count();
              $restricted = RestrictedDay::where('day',$order->date)->count();
	      $max_orders += $restricted * $cars;
              if($count_order >=$max_orders){
                  array_push($array, $order->date);
              }
          }
          return response()->Json(['disabled_days'=>$array]);
      }
}
