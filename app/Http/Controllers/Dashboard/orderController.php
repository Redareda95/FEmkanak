<?php

namespace App\Http\Controllers\dashboard;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Order;
use App\User;
use App\Car;
use App\assign_order;
use App\order_notification;
use DB;
class orderController extends Controller
{
    
    public function index()
    {
    	$orders = Order::orderBy('id', 'desc')->get();
            foreach ($orders as $order){
                if($order->user_id){ $order->employee_name = $this->get_order_employee($order->user_id);}
               if($order->service_id) { $order->service_name = $this->get_order_service($order->service_id);}
            }
    	return view('dashboard.orders.orders', compact('orders'));
    }

    public function get_order_service($service_id){
        $service = \App\Service::find($service_id);
        return $service->name;
    }
    public function get_order_employee($employee_id){
        $employee = \App\User::find($employee_id);
        return $employee->first_name.' '.$employee->last_name;
    }
    
    public function get_order_car($car_id){
        $car = \App\Car::find($car_id);
        return $car->name;
    }

    public function done_orders(){
        $orders = Order::where('status','Done')->get();
        foreach ($orders as $order){
             if($order->user_id){ $order->customer_name = $this->get_order_employee($order->user_id);}
             if($order->service_id){ $order->service_name = $this->get_order_service($order->service_id);}
             $order =  $this->get_assigned_order_data($order);
               }
            
        return view('dashboard.orders.done_orders', compact('orders'));
    }
    
    public function pending_orders(){
        $orders = Order::where('status','Pending')->get();
        foreach ($orders as $order){
                if($order->user_id){ $order->employee_name = $this->get_order_employee($order->user_id);}
               if($order->service_id) { $order->service_name = $this->get_order_service($order->service_id);}
            }
        return view('dashboard.orders.pending_orders', compact('orders'));
    }
    
    public function inprogress_orders(){
        $orders = Order::where('status','In progress')->get();
        foreach ($orders as $order){
                if($order->user_id){ $order->employee_name = $this->get_order_employee($order->user_id);}
               if($order->service_id) { $order->service_name = $this->get_order_service($order->service_id);}
               $order =  $this->get_assigned_order_data($order);
             
            }
        return view('dashboard.orders.inprogress_orders', compact('orders'));
    }
    
    public function rejected_orders(){
        $orders = Order::where('status','Rejected')->get();
        foreach ($orders as $order){
                if($order->user_id){ $order->employee_name = $this->get_order_employee($order->user_id);}
               if($order->service_id) { $order->service_name = $this->get_order_service($order->service_id);}
               $order =  $this->get_assigned_order_data($order);
             
            }
        return view('dashboard.orders.rejected_orders', compact('orders'));
    }

    public function get_assigned_order_data($order){
        $employee_name ="";
        $car_name ="";
        $assigned = assign_order::where('order_id',$order->id)->get()->first();
       if($assigned->user_id){ $employee_name = $this->get_order_employee($assigned->user_id);}
       if($assigned->car_id){$car_name = $this->get_order_car($assigned->car_id);}
        $order->employee_name = $employee_name;
        $order->car_name = $car_name;
        return $order;
    }
    public function edit($id){
        $order_seen = order_notification::where("order_id",$id)->get()->first();
        if($order_seen!=null){
        $order_seen->seen = '1';
        $order_seen->save();
        }        
    	$order = Order::find($id);
        $restricted = \App\RestrictedDay::where('day',$order->date)->get(['available_day_id']);
        $days = DB::table('available_days')->get();

        $employees = User::where("user_type","employee")->get();
    	return view('dashboard.orders.edit_orders', compact('order','employees', 'days'));
    }

    public function create()
    {
    	return view('dashboard.orders.add_orders');
    }
    
    public function get_avaliable_employees(){
    
    $pending_orders = \App\assign_order::join('orders','orders.id','=','assigned_order.order_id')
    ->where('status','=','In progress')
    ->get(['orders.id','assigned_order.user_id']);
    $employees  = \App\User::where('user_type','employee');
    
    foreach($pending_orders as $order){
        $employees=$employees->where('id','<>',$order->user_id);
    }
    
       return ($employees->get());
            }

    public function assign_order($id)
    {
         $order  = assign_order::where('order_id',$id)->get()->first();
         $order_id = $id;
        $is_pending = Order::where('id', $id)->where('status', 'Pending')->exists();
        if($is_pending === true)
        {
             $order  = Order::find($id);
             $cars  = Car::where('status','1')->get();
             $employees = $this->get_avaliable_employees();
        }else{
            session()->flash('deleted', 'Order Is Already Assigned');

           return redirect('/admin/orders');
        }
        // return $employees;
    	return view('dashboard.orders.assign_order', compact('order','cars','employees','order_id'));
    }
   
    
     public function assign_order_action(Request $request ,$id){
        
        $errors = array();
        
        if(!isset($request->employee_id)){
           $errors[] = 'Please Choose an Employee';
        }
        
        if(!isset($request->car_id)) {
            $errors[] = 'Please Choose a Car';
        }
        
        if(empty($errors)){
            //insert in assigned orders table 
            $employee_id = $request->input("employee_id");
            $car_id = $request->input("car_id");
            $order_id = $id;
            $assign_order = assign_order::firstOrNew(['order_id'=>$order_id]);
            $assign_order->user_id= $employee_id;
            $assign_order->car_id= $car_id;
            $assign_order->save();

            //change order status from pending to inprogress
            Order::where('id', $order_id)->update(['status'=> 'In progress']);

            //change car status
            Car::where('id', $car_id)->update(['status'=> '0']);

            session()->flash('message', 'Order Is Assigned Successfully');

            return redirect('/admin/orders');
        }else{
            return back()->withErrors('errors');
        }
        
    }

    public function update(Request $request, $id) 
    {
//    	$this->validate(request(),[
//            'time'=>'required',
//            'status'=>'required',
//            'date'=>'required',
//        ]);
        order::where('id', $id)->update($request->except(['_token', '_method']));

        session()->flash('message', 'order Is Updated');

        return redirect('/admin/orders');

    }

    public function store(Request $request) 
    {
//    	$this->validate(request(),[
//            'name'=>'required',
//            'desc'=>'required',
//        ]);

        $order = new Order;
        $order->time = $request->name;
        $order->date = $request->desc;
        $order->long = $request->desc;
        $order->lat = $request->desc;
        $order->status= $request->desc;
        $order->save();

        session()->flash('message', 'order Is Created');

        return redirect('/admin/order');

    }
    
     public function update_status(Request $request){
        $id = $request->input('id');
        $status = $request->input('status');
        if($status=="Done"||$status=="Rejected"){
            $assign_order = assign_order::where("order_id",$id)->get()->first();
            Car::where('id',$assign_order->car_id)->update(['status'=>1]);
        }
        Order::where('id',$id)->update(['status'=>$status]);
    }

}
