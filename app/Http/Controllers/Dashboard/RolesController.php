<?php

namespace App\Http\Controllers\Dashboard;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\Role;
use App\Order;
use App\Car;
use DB;

class RolesController extends Controller
{
    public function showPanel()
    {
        $orders = Order::orderBy('id', 'desc')->limit(5)->get();
        foreach ($orders as $order){
           $order->user = User::find($order->user_id);
        }
        $users = User::where('user_type', 'user')->orderBy('id', 'desc')->limit(8)->get();
        $cars = Car::all();

        $all_orders_count = Order::count(); 

        $done_orders_count = Order::where('status', 'Done')->count();

        $cancelled_orders_count = Order::where('status', 'Rejected')->count();

        $inprog_orders_count = Order::where('status', 'In progress')->count();
//        echo json_encode($orders);
    	return view('dashboard.home', compact('orders', 'users', 'cars', 'all_orders_count', 'done_orders_count', 'cancelled_orders_count','inprog_orders_count'));
    }
    
    public function showUsers()	
    {
    	$users = User::where('id', '!=' ,auth()->user()->id)->get();
    	
    	$roles = DB::table('roles')->get();
    	
        return view('dashboard.showUsers', compact('users', 'roles'));
    }

    public function addRole(Request $request)
    {
    	$user = User::where('email', $request['email'])->first();

        $user->roles()->detach();

        if($request['role_user'])
        {
            $user->roles()->attach(Role::where('name', 'user')->first());
        }

        if($request['role_editor'])
        {
            $user->roles()->attach(Role::where('name', 'editor')->first());
        }

        if($request['role_admin'])
        {
            $user->roles()->attach(Role::where('name', 'admin')->first());
        }

        return redirect()->back();
    }
}
