<?php

namespace App\Http\Controllers\Dashboard;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\ContactForm;

class ContactFormController extends Controller
{
    public function index()
	{
		$messages = ContactForm::all();

		return view('dashboard.messages.messages', compact('messages'));
	}

	public function show($id)
	{
		$message = ContactForm::find($id);
		$message->update(['read' => 'read']);
		
		return view('dashboard.messages.message', compact('message'));
	}
}
