<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Service;
use App\User;
use Auth;
use App\Order;
use App\public_texts;
use App\order_notification;

use App\verify_phone;
use SoapClient;
use SoapHeader;

class FrontController extends Controller
{
	public function __construct()
    {
        $this->middleware('auth');
    }
         
         
    public function Requests()
    {
        $user_id = Auth::user()->id;
        
        $previous_orders = Order::where('user_id', $user_id)->orderBy('created_at', 'desc')->paginate(10);
        
        return view('requests',compact('previous_orders'));
    }
    public function bookNow()
    {
        $public_texts = public_texts::get();

    	$services = Service::all();


    	$date =   date('Y-m-d', strtotime("+1 day"));

 if(verify_phone::where('user_id',Auth::user()->id)->get()->first()->verify == '0'){
            return view('verify_phone', compact('public_texts','date'));
        }
    	
    	return view('booking', compact('services', 'date', 'public_texts'));
    }

    public function SaveOrder(Request $request)
    {
	   	$this->validate(request(),[
	           'time'=>'required',
	           'date'=>'required',
	           'long'=>'required',
	           'lat'=>'required',
               'service_id'=>'required',
	       ]);

                $cars= \App\Car::count();
                $date =$request->date;
                $time =$request->time;
                $order_max = Order::where('date',$date)->where('time',$time)->count();

                if($order_max>=$cars)
                    { 
                        if($request->lang == 'en')
                        {
                            return back()->withErrors(['this time is not avalible for now']);
                        }else{
                            return back()->withErrors(['الوقت الذي اخترت محجوز الان']);
                        }
                    }

	        $order = new Order;
            $order->user_id = \Auth::user()->id;
	        $order->service_id = $request->service_id;
	        $order->time = $request->time;
	        $order->date = $request->date;
	        $order->long = $request->long;
	        $order->lat = $request->lat;
	        $order->status= "Pending";
	        $order->save();
                
                $nofity = new order_notification();
                $nofity->link = url('/').'/admin/edit_orders/'.$order->id;
                $nofity->order_text = 'New Order created at '.$order->created_at.' ';
                $nofity->order_id =$order->id;
                $nofity->save();
	        
            $order_max2 = Order::where('date',$date)->where('time',$time)->count();

            if($order_max2 == $cars)
            {
                $restict_order_date = new \App\RestrictedDay();
                $restict_order_date->day = $date;
                $restict_order_date->available_day_id = $time;
                $restict_order_date->save();
            }
                
		if($request->lang == 'en')
        {	
	        session()->flash('message', 'Order Is Reserved');
            
            return redirect('/?lang=en');

    	}else{
    		session()->flash('message', 'تم حجز طلبك');
            
            return redirect('/?lang=ar');
    		}
    }
    
    public function edit_profile(){
       $user = Auth::user();
       $public_texts = public_texts::get();

        return view('edit_profile', compact('user', 'public_texts'));
    }
    public static function englishTranslate($string){
        return explode('[EN]', $string)[0];
    }
    public static function arabicTranslate($string){
        return explode('[EN]', $string)[1];
    }
        public function upload_file(Request $request, $field_name) {
        $photoName = "profile.png";
        if ($request->hasFile($field_name)) {
            $ext = $request->$field_name->getClientOriginalExtension();
           $photoName = time() .'.'. $request->$field_name->getClientOriginalExtension();
           $request->$field_name->move('uploads/' , $photoName);
        }
        return $photoName;
    }

    public function edit_profile_action(Request $request){
       
        $id = Auth::user()->id;
        $request->validate([
            'first_name'=>'required',
            'last_name'=>'required',
            'email'=>'required|unique:users,email,'.$id.',id',
            'phone'=>'required',
        ]);
        $user = User::find($id);
        $user->first_name = $request->input('first_name');
        $user->last_name = $request->input('last_name');
        $user->email = $request->input('email');
        $user->phone = $request->input('phone');
        if($request->hasFile('file')){
        $user->image = 'uploads/'.$this->upload_file($request,'file');
        }
        $user->save();
        session()->flash('message', 'Profile Is Updated');

        return redirect('edit_profile');
    }

    public function servicePrice($id)
    {
    	$data = Service::where('id', $id)->value('price');

    	return response()->Json(['message' => $data]);
    }
    
    public function send_code(Request $request)
    {
        
        $phone = $request->input('phone');
        $user_id = $request->input('user_id');
        $user = \App\User::find($user_id);
        $user->phone=$phone;
        $user->save();
        
        $user_verify_phone = \App\verify_phone::where('user_id',$user_id)->get()->first();
        $pin_code =  rand(1000,9999);
        $user_verify_phone->phone = $phone;
        $user_verify_phone->pin_code =$pin_code;
        $user_verify_phone->save();
         $userName = "Zainagency";
         $Password = "iaKy3Ee6cM";
         $SMSText = "your pin code is ".$pin_code;
         $SMSLang = "e";
         $SMSSender = "Femkank";
         $SMSReceiver = $phone;
         $uri = "https://smsvas.vlserv.com/KannelSending/service.asmx?WSDL";
         
        $xml_data = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:tem="http://tempuri.org/">
        <soapenv:Header/>
        <soapenv:Body>
          <tem:SendSMS>
              <tem:UserName>'.$userName.'</tem:UserName>
              <tem:Password>'.$Password.'</tem:Password>
              <tem:SMSText>'.$SMSText.'</tem:SMSText>
              <tem:SMSLang>'.$SMSLang.'</tem:SMSLang>
              <tem:SMSSender>'.$SMSSender.'</tem:SMSSender>
              <tem:SMSReceiver>'.$SMSReceiver.'</tem:SMSReceiver>
          </tem:SendSMS>
        </soapenv:Body>
     </soapenv:Envelope>' ;
        // $URL = "https://test.testserver.com/PriceAvailability";
        $URL = "https://smsvas.vlserv.com/KannelSending/service.asmx?WSDL"; 
        
        $ch  = curl_init($URL);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: text/xml'));
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, "$xml_data");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $output = curl_exec($ch);
        curl_close($ch);
        if($output==0)
        {
            
            echo json_encode(1);
            // message sent successfully  ; 
        }
        else {
            
            echo json_encode(0);
            // Error
        }
        //  return response()->json("msg", str_replace(";", "", $response_arr));
        // after sms generated successfully ;
        // $send_success = 1 ;
        // if($send_success == 1){
        // }else {
        // }
    }
 
 public  function objectToArray($d)
 {
     if (is_object($d))
     {$d = get_object_vars($d);}
     if (is_array($d))
     {return array_map(__FUNCTION__, $d);}
     else {return $d;}
 }
 
 public function verfiy_phone(Request $request){
    $pin_code = $request->input('pin_code');
    $user_id =$request->input('user_id');
        $user_verify_phone = \App\verify_phone::where('user_id',$user_id)->get()->first();
        if($user_verify_phone->pin_code != $pin_code){
            echo json_encode("0");
        }else{
            $user_verify_phone->verify = 1;
            $user_verify_phone->save();
             echo json_encode("1");
        }
        
 }
}
