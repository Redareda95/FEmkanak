<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\ContactForm;
use App\SiteSetting;
use App\Service;
use App\order_notification;
use App\public_texts;
use Illuminate\Support\Facades\Mail;

class commonController extends Controller
{
    public function delete(Request $request){
        $table = $request->input('table');
        $id = $request->input('id');
        DB::table($table)->where('id',$id)->delete();
    }
    public function send_message(Request $request){
        $name = $request->input('name');
        $email = $request->input('email');
        $subject = $request->input('subject');
        $message = $request->input('message');
        $res = array();
        if(!$email || !$message || !$name){
        $res['res'] = 1;
        }else if(!filter_var($email, FILTER_VALIDATE_EMAIL) ){
            $res['res']=2;
        }
       else {
           $contact_message =  ContactForm::where('message',$message)->get();
           if($contact_message->count()){
               $res['res']=3;
           }else {
        $contact_message = new ContactForm();
        $contact_message->name = $name;
        $contact_message->email = $email;
        $contact_message ->subject = $subject;
        $contact_message ->message = $message;
        $contact_message ->read = "un_read";
        $contact_message ->save();
           $res['res']= 0;
           }
           
       }
        echo json_encode($res);
    }
    
    public static function englishTranslate($string){
        return explode('[EN]', $string)[0];
    }
    public static function arabicTranslate($string){
        return explode('[EN]', $string)[1];
    }

    public function index(){
        $public_texts = public_texts::get();
        $site_settings = SiteSetting::get()->first();
        $services = Service::all();
        return view('index',compact('site_settings' ,'services','public_texts'));
    }
    public function resend(){
        $email = session()->get('verify_email');
        $user = \App\User::where('email',$email)->get()->first();
        // return $user;
         Mail::send('email',array('user'=> $user,'token'=>$user->token),function($message){
            $message->to()->subject('Welcome to Fymkanak');
        });
        return back()->withErrors('email sent again please check mail');
        
    }
    public function get_notification(){
        $orders = order_notification::where('seen','0')->get();
        return response()->json([$orders]);
    }
  public function verfiy($token){
//         dd($token);
        $user = \App\User::where('token',$token)->update(['verify'=>'1']);
       
//        $user->save();
        return redirect('/login')->withErrors('verfication success you can now login');
    }

public static function today_orders(){
    $car_count = \App\Car::get()->count();
    $hours_count = \App\AvailableDay::get()->count();
    $day = Date("Y-m-d");
    $resticted_hours = \App\RestrictedDay::where('day',$day)->get()->count();
    $hours_count -=$resticted_hours;
    return $car_count * $hours_count;
} 

public static function today_requests(){
    $date = Date("Y-m-d");
    $orders_count = \App\Order::where('date',$date)->get()->count();
    return $orders_count;
} 
    
    
}
