<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class order_notification extends Model
{
    protected $table = "notification";
    public $timestamps = false;
            
}
