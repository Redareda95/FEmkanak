<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\AvailableDay;

class RestrictedDay extends Model
{
    public function available()
    {
		return $this->belongsTo(AvailableDay::class);	
    }
}
