<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\RestrictedDay;

class AvailableDay extends Model
{
    public function restricted()
    {
    	return $this->hasMany(RestrictedDay::class);
    }

    public function isRestricted($day)
    {
        if($this->restricted()->where('day', $day)->first())
        {
            return true;
        }

        return false;
        
    }
}
