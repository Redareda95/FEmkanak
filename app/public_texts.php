<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class public_texts extends Model
{
    protected $table = "public_texts";
    public $timestamps = false;
}
