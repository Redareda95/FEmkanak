<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Service;
use App\assign_order;

class Order extends Model
{
    protected  $table = "orders";
    public  $timestamps = true;
    
    public function user()
    {
    	return $this->belongsTo(User::class);
    }

    public function service()
    {
    	return $this->belongsTo(Service::class);
    }

    public function assigned()
    {
        return$this->hasOne(assign_order::class);
    }
}
