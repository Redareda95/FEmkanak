<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class assign_order extends Model
{
    protected  $table = "assigned_order";
    public $timestamps = true;

    protected $fillable = [
        'order_id'];
}
