<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('users')->delete();
        
        \DB::table('users')->insert(array (
            0 => 
            array (
                'id' => 1,
                'first_name' => 'Ahmed',
                'last_name' => 'Reda',
                'email' => 'reda19952013@gmail.com',
                'image' => 'uploads/1532527500.png',
                'password' => '$2y$10$MQxGlCSeWIPBv85FMTPSYebi4uUI10QHv0MkR1uIj/fpeRtYvxXMO',
                'phone' => '01114388500',
                'user_type' => 'admin',
                'remember_token' => 'ggItpjiDtNAJ9TmNGTWlh5B5nQX9VYU79yHaBRggDLj3LJ9TIuExpEcSDRUo',
                'created_at' => '2018-07-21 09:46:28',
                'updated_at' => '2018-07-25 14:05:00',
            ),
            1 => 
            array (
                'id' => 2,
                'first_name' => 'Test',
                'last_name' => 'Roles',
                'email' => 'reda@test.com',
                'image' => '',
                'password' => '123',
                'phone' => '2545',
                'user_type' => 'user',
                'remember_token' => NULL,
                'created_at' => '2018-07-09 22:00:00',
                'updated_at' => NULL,
            ),
            2 => 
            array (
                'id' => 4,
                'first_name' => 'Test',
                'last_name' => 'Roles',
                'email' => 'testreda@test.com',
                'image' => '',
                'password' => '123',
                'phone' => '2545',
                'user_type' => 'employee',
                'remember_token' => NULL,
                'created_at' => '2018-07-09 22:00:00',
                'updated_at' => NULL,
            ),
            3 => 
            array (
                'id' => 6,
                'first_name' => 'ahmed',
                'last_name' => 'gouda',
                'email' => 'ahmedmoud@ymail.com',
                'image' => 'uploads/1532519198.jpg',
                'password' => '$2y$10$9KO/NZl9Cc.GwC0FStD17.A7dK9MoqE6De9ill6ICru.OSvakno5m',
                'phone' => '01114123639',
                'user_type' => 'employee',
                'remember_token' => 'j6qiP1CIxCEFiD0u4LmP1suhv31gCan8AwFrj7I0EsFEj4X7FSEzDrUvGpup',
                'created_at' => '2018-07-23 10:27:29',
                'updated_at' => '2018-07-25 11:46:38',
            ),
            4 => 
            array (
                'id' => 8,
                'first_name' => 'mohamed',
                'last_name' => 'moud',
                'email' => 'mo@mo.com',
                'image' => '',
                'password' => '$2y$10$NlA42zZ0X0Rl4.iTOr10wOMm8SviuTksnrbamu6Y7lHqamz.zK1H2',
                'phone' => '12839173281',
                'user_type' => 'employee',
                'remember_token' => NULL,
                'created_at' => '2018-07-23 10:50:58',
                'updated_at' => '2018-07-23 10:50:58',
            ),
        ));
        
        
    }
}