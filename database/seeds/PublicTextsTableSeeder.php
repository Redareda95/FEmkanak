<?php

use Illuminate\Database\Seeder;

class PublicTextsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('public_texts')->delete();
        
        \DB::table('public_texts')->insert(array (
            0 => 
            array (
                'id' => 1,
                'name' => 'Working Hours',
                'value' => '6 AM To 8 PM SUNDAY CLOSED[EN] 6 صباحا الى 8 مسائا الاحد اجازة',
            ),
            1 => 
            array (
                'id' => 2,
                'name' => 'Phone',
                'value' => '+1-202-555-0137[EN]kldmf',
            ),
            2 => 
            array (
                'id' => 3,
                'name' => 'ADDRESS',
                'value' => '514 S. Magnolia St.Orlando , US[EN]kdmasmd',
            ),
            3 => 
            array (
                'id' => 4,
                'name' => 'E - MAIL',
                'value' => 'dummymail@mail.com[EN]dummymail@mail.com',
            ),
            4 => 
            array (
                'id' => 5,
                'name' => 'CALL US NOW',
            'value' => '(+1)202-202-012[EN](+1)202-202-012',
            ),
        ));
        
        
    }
}