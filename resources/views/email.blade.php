
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
            <meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0;">
                <title>Fymkank</title>

                <!-- Hotmail ignores some valid styling, so we have to add this -->
                <style type="text/css">
                    .ReadMsgBody {width: 100%; background-color: #ffffff;}
                    .ExternalClass {width: 100%; background-color: #ffffff;}
                    body {width: 100%; background-color: #ffffff; margin:0; padding:0; -webkit-font-smoothing: antialiased;}
                    html {width: 100%; }

                    @media only screen and (max-width: 640px) 
                    {
                        body{width:auto!important;}


                        table table               		{width:440px!important; }
                        .fullWidth						{width: 100%!important;}
                        .mobileCenter					{width: 440px !important; text-align: center!important; }
                        .headerScale					{width: 440px!important; height: 200px!important; border-radius: 4px!important;}
                        .eraseForMobile					{width:0; display:none !important;}
                        .navPad							{padding-right: 35px!important; padding-left: 35px; }
                        .headerBG						{width: 100%!important; height:auto!important;}
                        .headlineFont					{font-size: 16px!important; }
                        .headlinePad					{height: 14px!important; }
                        .textPad						{height: 6px!important; }
                        .smallImageScale				{width: 440px!important; height: 282px!important; border-radius: 4px!important;}
                        .imageScale1					{width: 440px!important; height: 230px!important; border-radius: 4px!important;}
                        .textLineHeight					{line-height: 22px!important;}
                        .imageScale2					{width: 440px!important; height: 232px!important; margin-top: 10px!important; }
                        .imageScale3					{width: 100px!important; height: auto!important; margin-top: 10px!important; }
                        .imageScale4					{width: 250px!important; height: auto!important; margin-top: 10px!important; margin-left: auto !important; margin-right: auto !important;}
                        .NoPadding						{padding: 0px!important; }
                        .image2PadMobile				{padding-top: 30px!important;}
                        .scaledown 						{width: 361px!important; padding-left: 10px!important;  }
                        .scaleDownPlusIcon 				{width: 14px!important;  }
                        .mobilePad						{height: 45px!important; }
                        .mobilePad3						{height: 30px!important; }
                        .mobilePad2						{height: 4px!important; }
                        .mobilePad4						{height:10px!important; }
                        .mobilePad5						{height:20px!important; }

                        .addForMobile 					{ height: 15px; }
                        table[class=table200]			{width:146px !important; text-align: center!important;}
                        table[class=tablefull]			{width:100% !important; padding-top: 25px;}

                        .roundCounter					{width: 58px!important; height: 58px!important; text-align: center; }
                        .roundCornerHeadline			{padding-top: 10px!important; padding-left: 0px!important; text-align: center!important;}
                        .fontFooter						{font-size: 13px!important; text-align: center!important; line-height: 22px!important;}
                        .footerCenter					{text-align: center!important; padding-bottom: 5px!important; }
                        .ExtraPadForMobile				{width: 191px; background-color: #f7f7f7; }
                        .footerLogoPad					{text-align: center!important; padding-top: 30px!important;}
                        .content						{display:inline-block; padding-left:2% !important; font-size:15px !important;}
                        .new							{display:none !important;}
                    }



                    @media only screen and (max-width: 479px) 
                    {
                        body{width:auto!important;}

                        table table 					{width: 280px!important; }
                        .fullWidth						{width: 100%!important;}
                        .mobileCenter					{width:280px !important; text-align: center!important; }
                        .headerScale					{width: 280px!important; height: 135px!important; border-radius: 4px!important;}
                        .eraseForMobile					{width:0; display:none !important;}
                        .navPad							{padding-right: 15px!important; padding-left: 15px; }
                        .headerBG 						{width: 100%!important; height: auto!important;}
                        .headlineFont					{font-size: 16px!important; }
                        .headlinePad					{height: 14px!important; }
                        .textPad						{height: 6px!important; }
                        .addForMobile 					{ height: 15px; }
                        .textLineHeight					{line-height: 22px!important;}
                        .smallImageScale				{width: 280px!important; height: 179px!important; border-radius: 4px!important;}


                        .imageScale1					{width: 280px!important; height: 150px!important; border-radius: 4px!important;}


                        .imageScale2					{width: 280px!important; height: 148px!important; border-radius: 4px!important; margin-top: 15px!important;}
                        .imageScale3					{width: 105px!important; height: auto!important; border-radius: 4px!important; margin-top: 5px!important;}
                        .imageScale4					{width: 200px!important; height: auto!important; border-radius: 4px!important; margin-top: 15px!important; margin-left:auto !important; margin-right:auto !important;}
                        .NoPadding						{padding: 0px!important; }
                        .image2PadMobile				{padding-top: 15px!important;}
                        .scaledown 						{width: 211px!important; padding-left: 10px!important;  }
                        .scaleDownPlusIcon 				{width: 14px!important;  }
                        .mobilePad						{height: 40px!important; }
                        .mobilePad2						{height: 4px!important; }
                        .mobilePad3						{height: 30px!important; }
                        .mobilePad4						{height: 20px!important; }
                        .mobilePad5						{height:15px!important; }

                        table[class=table200]			{width:93px !important; text-align: center!important;}
                        table[class=tablefull]			{width:100% !important; padding-top: 25px!important; }

                        .fontFooter						{font-size: 12px!important; text-align: center!important; line-height: 22px!important;}
                        .footerLogoPad					{text-align: center!important; padding-top: 40px!important;}

                        .headlineFont3					{text-align: center!important; width: 58px!important;}
                        .ExtraPadForMobile				{width: 111px; background-color: #f7f7f7; }
                        .content						{display:block !important;}
                        .new							{display:none !important;}
                    }


                </style>
                <body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" screen_capture_injected="true">

                    <!-- Wrapper -->
                    <table width="100%" border="0" cellpadding="0" cellspacing="0" align="center">
                        <tbody><tr>
                                <td width="100%" valign="top" bgcolor="#ffffff">

                                    <!-- Nav Mobile Wrapper -->
                                    <table width="580" border="0" cellpadding="0" cellspacing="0" align="center">
                                        <tbody><tr>
                                                <td width="100%" bgcolor="ffffff">

                                                    <!-- Start Nav -->
                                                    <table width="580" border="0" cellpadding="0" cellspacing="0" align="center">
                                                        <tbody><tr>
                                                                <td width="580" style="border-radius: 0 0 4px 4px!important;">

                                                                    <!-- Logo -->
                                                                    <table border="0" cellpadding="0" cellspacing="0" align="right" class="mobileCenter">
                                                                        <tbody>
                                                                            <tr>
                                                                                <td height="120" class="mobileCenter" style="text-align: left; font-family: Helvetica, Arial, sans-serif; line-height: 20px; vertical-align: middle;">
                                                                                    <a href="#" target="_blank"><img src="<?php echo url('/').'/images/slide3.jpg';?>" alt="CARE Egypt" border="0" class="headerBG"></a>
                                                                                </td>
                                                                            </tr>
                                                                        </tbody></table>

                                                                </td>
                                                            </tr>
                                                        </tbody></table><!-- End Nav -->

                                                </td>
                                            </tr>
                                        </tbody></table><!-- End Nav Mobile Wrapper -->

                                    <!-- Mobile Wrapper -->
                                    <table width="580" border="0" cellpadding="0" cellspacing="0" align="center" style="background-color:#f5f5f5;">
                                        <tbody><tr mc:repeatable="">
                                                <td width="100%">
                                                    <div mc:hideable=""><!-- MC Hideable -->

                                                        <table class="eraseForMobile" width="580" border="0" cellpadding="0" cellspacing="0" align="center">
                                                            <tbody><tr>
                                                                    <td height="30">									
                                                                    </td>
                                                                </tr>
                                                            </tbody></table>

                                                        <!-- Start Text Left, Image Right -->
                                                        <table width="580" border="0" cellpadding="0" cellspacing="0" align="center">
                                                            <tbody><tr>
                                                                    <td width="580" class="mobileCenter">

                                                                        <!-- Image 4 Width= 267px, Height= 141px!! --><!-- End Image 4 -->

                                                                        <!-- Padding + (Outlook) --><!-- Image 1 Width= 267px, Height= 141px!! -->
                                                                        <table class="NoPadding" align="left" cellpadding="0" cellspacing="0" border="0">

                                                                            <tbody><tr>
                                                                                    <td valign="top" height="0" align="center" class="addForMobile"></td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td valign="top" height="30" align="center">

                                                                                        <!-- Headline 1 -->
                                                                                        <table class="mobileCenter" width="580" border="0" cellpadding="0" cellspacing="0" align="center">
                                                                                            <tbody>
                                                                                                <tr>
                                                                                                    <td mc:edit="image4Headline" class="headlineFont" style="font-size: 20px; color: #808080; font-weight: normal; text-align: left; font-family: Helvetica, Arial, sans-serif; font-size:15px; line-height: 24px; vertical-align:top; padding-left:20px; padding-right:20px;">
                                                                                                        <h1 style="font-size:22px; color:#4f2d5f; font-weight: normal; text-align: left; font-family:Helvetica, Arial, sans-serif; line-height: 24px; vertical-align:top; direction:ltr;" class="textLineHeight">Thank you for registering to FEmkanak for car services.</h1>
                                                                                                        <p style="font-size:15px; color: #6e6e6e; font-weight: normal; text-align: left; font-family:Helvetica, Arial, sans-serif; line-height: 24px; vertical-align:top; direction:ltr;" class="textLineHeight">
                                                                                                            Dear {{$user->first_name}},<br />
                                                                                                            Thank you for your interest in FEmkanak.<br />
                                                                                                            As part of validating your registration, you are kindly requested to click on the below link for confirming your email account:</p>

                                                                                                        <p style="color: #6e6e6e; font-weight: normal; text-align: left; font-family:Helvetica, Arial, sans-serif; line-height: 24px; vertical-align:top; direction:ltr;" class="textLineHeight">
                                                                                                            <a href="{{url('verfiy').'/'.$token}}" target="_blank" style="color:#49BDC6;">Verification Link Goes Here</a></p>

                                                                                                        <p style="color: #6e6e6e; font-weight: normal; text-align: left; font-family:Helvetica, Arial, sans-serif; line-height: 24px; vertical-align:top; direction:ltr;" class="textLineHeight">Please note that this is essential for completing your registration process. </p>

                                                                                                        Sincerely,<br />
                                                                                                        <a href="{{url('/')}}" target="_blank" style="font-size:15px; color:#49BDC6; font-weight: normal; font-family:Helvetica, Arial, sans-serif; line-height: 24px; vertical-align:top; direction:rtl;">FEmkanak.com</a></p>							
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td class="mobilePad2" height="20" style="font-size: 0;line-height: 0;border-collapse: collapse;"></td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td class="mobilePad2" height="10" style="font-size: 0;line-height: 0;border-collapse: collapse; background:#ddd;"></td>
                                                                                                </tr>
                                                                                            </tbody></table>


                                                                                    </td>
                                                                                </tr>
                                                                            </tbody></table><!-- End Image 1 -->


                                                                    </td>
                                                                </tr>
                                                            </tbody></table><!-- End Text Left, Image Right -->	

                                                    </div></td>
                                            </tr>
                                        </tbody></table><!-- End MC Repeatable --><!-- End Mobile Wrapper -->

                                </td>
                            </tr>
                        </tbody></table><!-- End Wrapper -->

                    <!-- Done -->




                </body></head></html>