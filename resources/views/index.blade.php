@extends('master')
@section('content')
@php
  $lang = (isset($_GET['lang'])) ? $_GET['lang'] : 'en' ;
@endphp 
<?php
use App\Http\Controllers\commonController;
?>
<!------ Slider Start ------>
    
             
    <video  id="myvedio" playsinline autoplay="autoplay" loop muted style="width:100%">
            <source src="landing_video.mp4" type="video/mp4">
    </video>
            
@if($flash = session('message'))
    <script>
        alert("{{ $flash }}");
    </script>
@endif
@if($flash = session('deleted'))
    <script>
        alert("{{ $flash }}");
    </script>
@endif
    @if($lang == 'ar')
    <br> 
    <center>
    <a href="/book_now?lang=ar" class="impl_btn">  احجز الان </a>
    </center>
    <br>
    <!------ Latest Blog Section Start ------>
    <div class="impl_blog_wrapper">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12">
                    <div class="impl_blog_box">
                        <div class="row">
                            <div class="col-lg-4 col-md-12">
                                <div class="impl_post_img">
                                    <img src="images\blog\post1.jpg" alt="" class="img-fluid">
                                    
                                    <div class="impl_pst_img_icon"><a href="/book_now?lang={{ $lang }}" class="impl_btn">  احجز الان </a></div>
                                </div>
                            </div>
                            <div class="col-lg-8 col-md-12">
                                <br>
                                <div class="impl_heading">
                                        <h1 style="color: #000">عن الشركة</h1>
                                    </div>
                                <div class="impl_post_data">
                                    <?php echo $site_settings->about_company_ar;?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!------ Service and Video Wrapper Start ------>
   
    <!------ Purchase Car Start ------>
    <div class="impl_spesi_wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12 col-md-12">
                    <div class="impl_car_spesi_list">
                        <div class="impl_heading">
                            <h1>خدماتنا</h1>
                        </div>
                        <ul>
                            @foreach($services as $service)
                            <li>
                                <h3>{{ $service->name }}</h3>
                                <div class="progress">
                                    <div class="progress-bar" style="width:70%"></div>
                                </div>
                            </li>
                            @endforeach
                        </ul>
                    </div>
                </div>
            </div>

        </div>
    </div>
    
    <!------ Contact Wrapper Start ------>
    <div class="impl_contact_wrapper" id="contact">
        <div class="container">
            <div class="row">
                <div class="col-lg-10 col-md-12 offset-lg-1">
                    <div class="impl_con_form">
                        <div class="contact_map">
                            <div id="contact_map"></div>
                        </div>
                        <div class="col-lg-12 col-md-12">
                            <h1 style="color: #fff">اتصل بنا</h1>
                        </div>
                        <form onsubmit="checkMessage()" method="POST" action="{{url('/send_message')}}" id="contactForm">
                            {!!csrf_field()!!}
                            
                            <div id="success" style="color:#33ce67; font-size: x-large;"></div>
                            <div id="fail" style="color:red; font-size: x-large;"></div>
                            <div class="col-lg-12 col-md-12">
                                <div class="form-group">
                                    <input type="text" name="name" class="form-control require" placeholder="الأسم">
                                </div>
                            </div>
                            <div class="col-lg-12 col-md-12">
                                <div class="form-group">
                                    <input type="text" name="email" class="form-control require" placeholder="الايميل" data-valid="email" data-error="Email should be valid.">
                                </div>
                            </div>
                            <div class="col-lg-12 col-md-12">
                                <div class="form-group">
                                    <input type="text" name="subject" class="form-control" placeholder="الموضوع">
                                </div>
                            </div>
                            <div class="col-lg-12 col-md-12">
                                <div class="form-group">
                                    <textarea name="message" class="form-control" placeholder="الرسالة"></textarea>
                                </div>
                            </div>
                            <div class="response"></div>
                            <div class="col-lg-12 col-md-12">
                                <input type="hidden" name="form_type" value="contact">
                                <button type="submit" class="impl_btn submitForm">أرسل</button>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="col-lg-12 col-md-12">
                    <div class="impl_contact_info">
                        <div class="row">
                            <div class="col-lg-4 col-md-4">
                                <div class="impl_contact_box">
                                    <div class="impl_con_data">
                                        <i class="fa fa-phone" aria-hidden="true"></i>
                                        <h2>الهاتف</h2>
                                     {{commonController::arabicTranslate($public_texts[1]->value)}}
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-4">
                                <div class="impl_contact_box">
                                    <div class="impl_con_data">
                                        <i class="fa fa-home" aria-hidden="true"></i>
                                        <h2>العنوان</h2>
                                        {{commonController::arabicTranslate($public_texts[2]->value)}}
                                    
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-4">
                                <div class="impl_contact_box">
                                    <div class="impl_con_data">
                                        <i class="fa fa-envelope" aria-hidden="true"></i>
                                        <h2>الايميل</h2>
 {{commonController::arabicTranslate($public_texts[3]->value)}}
                                    
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    <!------ Need Help Section Start ------>
    <div class="impl_help_wrapper">
        <div class="container">
            <div class="row">
                <div class="col-lg-10 col-md-12 offset-lg-1">
                    <div class="impl_help_data">
                        <h1>للمزيد من المعلومات بالرجاء  </h1>
                        <p style="color: white">اتصل بنا الان</p>
                        <div class="impl_help_no"><span> {{commonController::arabicTranslate($public_texts[4]->value)}}</span></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @else
    <br>
    <center>
        <a href="/book_now?lang={{ $lang }}" class="impl_btn"> Book Now </a>
    </center>
    <br>
    <!------ Latest Blog Section Start ------>
    <div class="impl_blog_wrapper">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12">
                    <div class="impl_blog_box">
                        <div class="row">
                            <div class="col-lg-4 col-md-12">
                                <div class="impl_post_img">
                                    <img src="images\blog\post1.jpg" alt="" class="img-fluid">
                                    
                                    <div class="impl_pst_img_icon"> <a href="/book_now?lang={{ $lang }}" class="impl_btn"> BOOK NOW </a> </div>
                                </div>
                            </div>
                            <div class="col-lg-8 col-md-12">
                                <br>
                                <div class="impl_heading">
                                    <h1 style="color: #000">ABOUT US</h1>
                                </div>
                                <div class="impl_post_data">
                                    <?php echo $site_settings->about_company;?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!------ Service and Video Wrapper Start ------>
   
    <!------ Purchase Car Start ------>
    <div class="impl_spesi_wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12 col-md-12">
                    <div class="impl_car_spesi_list">
                        <div class="impl_heading">
                            <h1>Our Services</h1>
                        </div>
                        <ul>
                            @foreach($services as $service)
                            <li>
                                <h3>{{ $service->name }}</h3>
                                <div class="progress">
                                    <div class="progress-bar" style="width:70%"></div>
                                </div>
                            </li>
                            @endforeach
                        </ul>
                    </div>
                </div>
            </div>

        </div>
    </div>
    
    <!------ Contact Wrapper Start ------>
    <div class="impl_contact_wrapper" id="contact">
        <div class="container">
            <div class="row">
                <div class="col-lg-10 col-md-12 offset-lg-1">
                    <div class="impl_con_form">
                        <!--<div class="contact_map">-->
                        <!--    <div id="mapRed"></div>-->
                        <!--</div>-->
                        <div class="col-lg-12 col-md-12">
                            <h1 style="color: #fff">Get In Touch</h1>
                        </div>
                        <form  method="POST" action="{{url('/send_message')}}" id="contactForm">
                            {!!csrf_field()!!}
                            
                            <div id="success" style="color:#33ce67; font-size: x-large;"></div>
                            <div id="fail" style="color:red; font-size: x-large;"></div>
                            <div class="col-lg-12 col-md-12">
                                <div class="form-group">
                                    <input type="text" name="name" class="form-control require" placeholder="YOUR NAME">
                                </div>
                            </div>
                            <div class="col-lg-12 col-md-12">
                                <div class="form-group">
                                    <input type="text" name="email" class="form-control require" placeholder="YOUR EMAIL" data-valid="email" data-error="Email should be valid.">
                                </div>
                            </div>
                            <div class="col-lg-12 col-md-12">
                                <div class="form-group">
                                    <input type="text" name="subject" class="form-control" placeholder="SUBJECT">
                                </div>
                            </div>
                            <div class="col-lg-12 col-md-12">
                                <div class="form-group">
                                    <textarea name="message" class="form-control" placeholder="YOUR MESSAGE"></textarea>
                                </div>
                            </div>
                            <div class="response"></div>
                            <div class="col-lg-12 col-md-12">
                                <input type="hidden" name="form_type" value="contact">
                                <button type="submit" class="impl_btn submitForm">Send Message</button>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="col-lg-12 col-md-12">
                    <div class="impl_contact_info">
                        <div class="row">
                            <div class="col-lg-4 col-md-4">
                                <div class="impl_contact_box">
                                    <div class="impl_con_data">
                                        <i class="fa fa-phone" aria-hidden="true"></i>
                                        <h2>phone</h2>
                                         {{commonController::englishTranslate($public_texts[1]->value)}}
                                    
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-4">
                                <div class="impl_contact_box">
                                    <div class="impl_con_data">
                                        <i class="fa fa-home" aria-hidden="true"></i>
                                        <h2>address</h2>
                                        
                                         {{commonController::englishTranslate($public_texts[2]->value)}}
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-4">
                                <div class="impl_contact_box">
                                    <div class="impl_con_data">
                                        <i class="fa fa-envelope" aria-hidden="true"></i>
                                        <h2>E - mail</h2>
                                        
                                         {{commonController::englishTranslate($public_texts[3]->value)}}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    <!------ Need Help Section Start ------>
    <div class="impl_help_wrapper">
        <div class="container">
            <div class="row">
                <div class="col-lg-10 col-md-12 offset-lg-1">
                    <div class="impl_help_data">
                        <h1>Need Help Finding Perfect Car Service ?</h1>
                        <p style="color: white">Call Us Now</p>
                        <div class="impl_help_no"><span>
                                         {{commonController::englishTranslate($public_texts[4]->value)}}</span></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @endif
    <script>
        var vedio = document.getElementById('myvedio');
        
        function enableAutoplay() {
            vedio.autoplay = true;
            vedio.load();
        }
        
        
    </script>
    @stop