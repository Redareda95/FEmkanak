 @php
  $lang = (isset($_GET['lang'])) ? $_GET['lang'] : 'en' ;
  $settings = \App\SiteSetting::find(1);
@endphp 
 <div class="impl_footer_wrapper">
        <div class="impl_social_wrapper">
            <ul>
                <li><a href="{{$settings->fb_link}}"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                <li><a href="{{$settings->twitter_link}}"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                <li><a href="{{$settings->google_plus}}"><i class="fa fa-google-plus" aria-hidden="true"></i></a></li>
                <li><a href="{{$settings->youtube_link}}"><i class="fa fa-youtube-play" aria-hidden="true"></i></a></li>
                <li><a href="{{$settings->linkedin_link}}"><i class="fa fa-rss" aria-hidden="true"></i></a></li>
                <li><a href="{{$settings->linkedin_link}}"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
                <li><a href="{{$settings->fb_link}}"><i class="fa fa-pinterest-p" aria-hidden="true"></i></a></li>
                <li><a href="{{$settings->instgram_link}}"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
            </ul>
        </div>
    </div>
    <!----Bottom Footer Start---->
    <div class="impl_btm_footer">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12">
                    <p class="impl_copyright" style="color: white">Copyright &copy; {{ date('Y') }} {{ strtoupper($settings->site_name) }}. All Rights Reserved to <a href="http://gtsaw.com" style="color: #e01b49">GTSAW</a></p>
                </div>
            </div>
        </div>      
    </div>   
    <!---- Go To Top---->
    <!--<span class="gotop"><img src="\images\car.png" alt=""></span>-->
    <!--Main js file Style-->
    <script type="text/javascript" src="\js\jquery.js"></script>
    <script type="text/javascript" src="\js\login_ajax.js"></script>
    <script type="text/javascript" src="\js\popper.js"></script>
    <script type="text/javascript" src="\js\bootstrap.min.js"></script>
    <script type="text/javascript" src="\js\ion.rangeSlider.min.js"></script>
    <script type="text/javascript" src="\js\plugin\magnific\jquery.magnific-popup.min.js"></script>
    <script type="text/javascript" src="\js\plugin\slick\slick.min.js"></script>
    <script type="text/javascript" src="\js\plugin\nice_select\jquery.nice-select.min.js"></script>
    <!----------Revolution slider start---------->
    <script type="text/javascript" src="\js\plugin\revolution\js\jquery.themepunch.revolution.min.js"></script>
    <script type="text/javascript" src="\js\plugin\revolution\js\jquery.themepunch.tools.min.js"></script>
    <script type="text/javascript" src="\js\plugin\revolution\js\revolution.extension.kenburn.min.js"></script>
    <script type="text/javascript" src="\js\plugin\revolution\js\revolution.extension.layeranimation.min.js"></script>
    <script type="text/javascript" src="\js\plugin\revolution\js\revolution.extension.navigation.min.js"></script>
    <script type="text/javascript" src="\js\plugin\revolution\js\revolution.extension.parallax.min.js"></script>
    <script type="text/javascript" src="\js\plugin\revolution\js\revolution.extension.slideanims.min.js"></script>
    <script type="text/javascript" src="\js\plugin\revolution\js\revolution.extension.actions.min.js"></script>
    <script type="text/javascript" src="\js\plugin\revolution\js\revolution.addon.slicey.min.js"></script>
    <!----------Revolution slider start---------->
    <script type="text/javascript" src="\js\custom.min.js"></script>
    <script type="text/javascript" src="\js\custom.js"></script>
    <!--Main js file End-->
  
   <script>
 function initMap() {
  var uluru = {lat: 30.008409, lng: 31.429195};
  var map = new google.maps.Map(document.getElementById('mapRed'), {
    zoom: 17,
    center: uluru
  });
  var geocoder = new google.maps.Geocoder();

  var contentString = ' ';

  var infowindow = new google.maps.InfoWindow({
    content: contentString
  });

  var marker = new google.maps.Marker({
    position: uluru,
    map: map,
    title: 'Uluru (Ayers Rock)'
  });
  marker.addListener('click', function() {
    infowindow.open(map, marker);
  });
}

</script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyA3Yj4XQ1rNfsgb1heGuRW95ZPLrSs75lo&callback=initMap"
    async defer></script>
    <script>
    
    $(document).ready(function(){
 $('.pop-up').on('click', function(){
    $('#overlay').fadeIn(300); 
    $('.calendar').fadeIn(300); 
    let clickedbutton = $("input",$(this).parent()).attr('id');
    $('.dates').data('type',clickedbutton);

 });
 
 $('table').on('click', function(event){
   let that=$(event.target);
    if(that.is('td') && !that.hasClass('notCurMonth') && !that.hasClass('holiday') && !that.hasClass('curDay')){
        $('td.curDay').toggleClass('curDay');
        that.toggleClass('curDay');
    }
}); 

$('#add_event').on('click', function(){
    let value= $('td.curDay').html();
    $('#overlay').fadeOut(300);
    $('.calendar').fadeOut(300);
    let id=($('.dates').data()).type;
    $('#' + id).val(value+" {{date('M, Y')}}");
}); 

$('#search').on('click', function(e){
    $('.booking').addClass('is-sent');
    e.preventDefault();
});
}); 
       
function readURL(input) {
  if (input.files && input.files[0]) {
    var reader = new FileReader();
    reader.onload = function(e) {
      $('#img_profile').attr('src', e.target.result);      
    }
    reader.readAsDataURL(input.files[0]);
  }
}

$("#profile_image").change(function() {
 $('#img_profile').show();
        readURL(this);
});

    </script>
</body>

</html>