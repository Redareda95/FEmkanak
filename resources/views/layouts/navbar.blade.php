@php
  $lang = (isset($_GET['lang'])) ? $_GET['lang'] : 'en' ;
  $settings = \App\SiteSetting::find(1);
@endphp 

<?php 
use App\Http\Controllers\commonController;
?>
<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->
<!-- Begin Head -->

<head>
    <title>{{ $settings->site_name }}</title>
    <meta charset="utf-8">
    <meta content="width=device-width, initial-scale=1.0" name="viewport">
    <meta name="description" content="{{ $settings->description }}">
    <meta name="keywords" content="{{ $settings->keywords }}">
    <meta name="author" content="{{ $settings->author }}">
    <meta name="MobileOptimized" content="320">
    <!--Srart Style -->
    <link rel="stylesheet" type="text/css" href="\css\fonts.css">
  <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="stylesheet" type="text/css" href="\css\bootstrap.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

    <link rel="stylesheet" type="text/css" href="\css\font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="\css\ion.rangeSlider.css">
    <link rel="stylesheet" type="text/css" href="\css\ion.rangeSlider.skinFlat.css">
    <link rel="stylesheet" type="text/css" href="\js\plugin\magnific\magnific-popup.css">
    <link rel="stylesheet" type="text/css" href="\js\plugin\slick\slick.css">
    <link rel="stylesheet" type="text/css" href="\js\plugin\slick\slick-theme.css">
    <link rel="stylesheet" type="text/css" href="\js\plugin\nice_select\nice-select.css">
    <link href="https://fonts.googleapis.com/css?family=Lobster" rel="stylesheet">
     <!----Revolution slider---->
    <link rel="stylesheet" type="text/css" href="\js\plugin\revolution\css\settings.css">
    <link rel="stylesheet" type="text/css" href="\css\style.min.css">
    @if($lang == 'en')
    <link rel="stylesheet" type="text/css" href="/css/color.css">
    @else
    <link rel="stylesheet" type="text/css" href="/css/arabic.css">
   
    @endif
    <!-- Favicon Link --> 
    <link rel="icon" type="image/png" href="http://testfymkanak.hatgwez.com/uploads/{{ $settings->favicon}}">
    
</head>

<body onload="enableAutoplay()">
    <!------ Preloader Start ------>
<!--    <div id="loading">-->
<!--  <div id="loading-center">-->
<!--      <img src="\images\loader1.gif" alt="">-->
<!-- </div>-->
<!--</div> -->

   
    <!------ Header Start ------>
    <div class="impl_header_wrapper">
        <div class="impl_logo">
            <a href="/?lang={{ $lang }}"><img src="/uploads/{{ $settings->logo }}" width="200px" alt="Logo" class="img-fluid"></a>
        </div>
        <div class="impl_top_header" >
            <div class="container">
                <div class="row">
                    <div class="col-lg-12 col-md-12">
                        <div class="impl_top_info">
                            <!--@if($lang == 'en')-->
                            <!--<p class="impl_header_time"><i class="fa fa-clock-o" aria-hidden="true"></i>@if(isset($public_texts[0]->value)){{commonController::englishTranslate($public_texts[0]->value)}}@endif</p>-->
                            <!--@else-->
                            <!--<p class="impl_header_time"><i class="fa fa-clock-o" aria-hidden="true"></i>@if(isset($public_texts[0]->value)){{commonController::arabicTranslate($public_texts[0]->value)}}@endif</p>-->
                            <!--@endif-->
                            @if($lang == 'en')
                            <ul class="impl_header_icons" style="color: black">
                                <li><button class="btn btn-link" ><a href="?lang=ar"  style="color: black"><i class="fa fa-language" aria-hidden="true"></i> Arabic </a></button></li>
                                @if(Auth::check())
                                <li><form action="/logout?lang={{ $lang }}" method="POST">
                                        {{ csrf_field() }}
                                        <button type="submit" class="btn btn-link" style="color: black"><i class="fa fa-sign-out" aria-hidden="true" style="color: black"></i> Sign Out</button>
                                      </form>
                                  </li>
                                  @else
                                <li><a href="/login?lang={{ $lang }}" style="color: black"><button class="btn btn-link" ><i class="fa fa-sign-in" aria-hidden="true" style="color: black"></i> Login / Register</button></a></li>
                                @endif
                                
                            </ul>
                            @else
                            <ul class="impl_header_icons" style="color: black">
                                <li><button class="btn btn-link" ><a href="?lang=en"  style="color: black"><i class="fa fa-language" aria-hidden="true"></i> English </a></button></li>
                                @if(Auth::check())
                                <li><form action="/logout" method="POST">
                                        {{ csrf_field() }}
                                        <button type="submit" class="btn btn-link" style="color: black"><i class="fa fa-sign-out" aria-hidden="true" style="color: black"></i> تسجيل خروج</button>
                                      </form>
                                  </li>
                                  @else
                                <li><a href="/login?lang={{ $lang }}" style="color: black"><button class="btn btn-link" ><i class="fa fa-sign-in" aria-hidden="true" style="color: black"></i> تسجيل دخول / أنشاء حساب</button></a></li>
                                @endif
                            </ul>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--menu start-->
        <div id="sidebar_menu" class="impl_menu_wrapper">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12 col-md-12">
                        <button class="impl_menu_btn">
			<i class="fa fa-car" aria-hidden="true"></i>
			<i class="fa fa-bars" aria-hidden="true"></i>
		</button>
                        <div class="impl_menu_inner">
                            <div class="impl_logo_responsive">
                                <a href="/?lang={{ $lang }}"><img src="/uploads/{{ $settings->logo }}" width="200px" alt="Logo" class="img-fluid"></a>
                            </div>
                            @if(Auth::check())
                            <div class="impl_menu">
                                <nav class="hidden-sm hidden-xs">
                                    <div class="menu_cross" >
                                        <i class="fa fa-times" aria-hidden="true"></i>
                                    </div>
                                    <ul>
                                        <li style="padding-right: 10px; float: right !important;">
                                            <div class="profile-link" href="#" title="More about {{Auth::user()->first_name}}">
                                              <img class="img-circle" src="@if(Auth::user()->image){{Auth::user()->image}} @else {{ '/uploads/profile.jpg' }}@endif"/>
                                            </div>
                                        </li>
                                        <li style="margin-top: 20px"><a href="/?lang={{ $lang }}">@if($lang == 'en'){{'Home'}}@else {{'الصفحة الرئيسية'}} @endif</a></li>
                                        <li style="margin-top: 20px"><a href="#">{{ Auth::user()->first_name }} {{ Auth::user()->last_name }}</a></li>
                                        <li style="margin-top: 20px"><a href="/requests?lang={{$lang}}">@if($lang == 'en'){{'Your Requests'}}@else{{'طلباتك السابقة'}} @endif</a> </li>
                                        @if($lang == 'en')  
                                        <li style="margin-top: 20px; "><a href="edit_profile?lang={{ $lang }}">Edit Profile</a> </li>
                                        <li style="margin-top: 20px; " ><a href="/book_now?lang={{ $lang }}">Book Now</a> </li>

                                        @else
                                         <li style="margin-top: 20px;" ><a href="edit_profile?lang={{ $lang }}">تعديل بيانات</a> </li>
                                        <li style="margin-top: 20px; "><a href="/book_now?lang={{ $lang }}">احجز الان</a> </li>
                                        @endif
                                    </ul>
                                </nav>

                                <!-- mobile -->
                                 <nav class="hidden-lg hidden-md">
                                    <div class="menu_cross" style="background-color: transparent !important;">
                                         <i class="fa fa-times" aria-hidden="true"></i>
                                         <!--<h1 style="color: #e01d48; font-family: 'Lobster', cursive !important; border-bottom: 1px #000 double" class="pull-left">{{ $settings->site_name }}</h1>-->
                                    </div>
                                    <ul>
                                        <li style="top: 10px;left: -65px;">
                                            <div class="profile-link" href="#" title="More about {{Auth::user()->first_name}}">
                                              <img class="img-circle" src="@if(Auth::user()->image){{Auth::user()->image}} @else {{ '/uploads/profile.jpg' }}@endif" style="width:70%"/>
                                            </div>
                                        </li>
                                        <li style="margin-top: 20px;"><a href="#" style="@if($lang=='en'){{"top: -60px; left: 105px; font-size: 13px;color: black;"}}@else {{"top: -60px;left: 48px;font-size: 13px;color: black;"}} @endif">{{ Auth::user()->first_name }} {{ Auth::user()->last_name }}</a></li>
                                        
                                        <li style="margin-top: 20px"><a href="/?lang={{ $lang }}">@if($lang == 'en'){{'Home'}}@else {{'الصفحة الرئيسية'}} @endif</a></li>
                                        
                                        <li style="margin-top: 20px"><a href="/requests?lang={{$lang}}">@if($lang == 'en'){{'Your Requests'}}@else{{'طلباتك السابقة'}} @endif</a> </li>

                                 @if($lang == 'en')  
                                        <li style="margin-top: 20px; "><a href="edit_profile?lang={{ $lang }}" >Edit Profile</a> </li>
                                        <li style="margin-top: 20px; " ><a href="/book_now?lang={{ $lang }}">Book Now</a> </li>

                                        @else
                                         <li style="margin-top: 20px;" ><a href="edit_profile?lang={{ $lang }}">تعديل بيانات</a> </li>
                                        <li style="margin-top: 20px; "><a href="/book_now?lang={{ $lang }}">احجز الان</a> </li>
                                        @endif
                                    </ul>
                                </nav>
                            </div>
                            @else
                            <div class="impl_menu">
                                <nav>
                                      <div class="menu_cross" style="background-color: transparent !important;">
                                         <i class="fa fa-times" aria-hidden="true"></i>
                                         <h1 style="color: #e01d48; font-family: 'Lobster', cursive !important; border-bottom: 1px #000 double" class="pull-left">{{ $settings->site_name }}</h1>
                                    </div>
                                    <ul>      
                                    @if($lang == 'en')                                  
                                        <li style="margin-top: 20px"><a href="/?lang={{ $lang }}">Home</a></li>
                                        <li style="margin-top: 20px"><a href="#contact">Contact Us</a> </li>
                                        <li style="margin-top: 20px;">   <a href="/book_now?lang={{ $lang }}">Book Now</a>
                                        </li>
                                    @else
                                        <li style="margin-top: 20px"><a href="/?lang={{ $lang }}">الصفحة الرئيسية</a></li>
                                        <li style="margin-top: 20px"><a href="#contact">أتصل بنا</a> </li>
                                        <li style="margin-top: 20px"> <a href="/book_now?lang={{ $lang }}"  >احجز الان</a></li>
                                    @endif
                                    </ul>
                                </nav>
                            </div>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>