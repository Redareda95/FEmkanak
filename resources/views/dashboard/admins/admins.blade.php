@extends('dashboard.master')
@section('content')
<style>
    .exportExcel{
  padding: 5px;
  border: 1px solid grey;
  margin: 5px;
  cursor: pointer;
}
</style>
@if($flash = session('message'))
<div class="alert success">
  <span class="closebtn" onclick="this.parentElement.style.display='none';">&times;</span> 
    <b>{{ $flash }}</b>
</div>
@endif
@if($flash = session('deleted'))
<div class="alert">
  <span class="closebtn" onclick="this.parentElement.style.display='none';">&times;</span> 
    <b>{{ $flash }}</b>
</div>
@endif

<div class="box">
            <div class="box-header">
  <h3 class="box-title">Data Table For Site Super Admins</h3>
</div>
    <div class="box-body">
      <table id="example1" class="table table-bordered table-striped">
        <thead>
        <tr>
          <th>ID</th>
          <th>First Name</th>
          <th>Last Name</th>
          <th>Email</th>
          <th>Phone #</th>
          <th>Date of Register</th>
          <th>Last Login to System</th>
          <th>Edit / Delete</th>
        </tr>
        </thead>
        <tbody>
@foreach($admins as $admin)
        <tr table="users" id="{{$admin->id}}">
          <td>{{ $admin->id }}</td>
          <td>{{ $admin->first_name }}</td>
          <td>{{ $admin->last_name }}</td>
          <td>{{ $admin->email }}</td>
          <td>{{ $admin->phone }}</td>
          <td>{{ $admin->created_at->toFormattedDateString() }}</td>
          <td>{{ $admin->updated_at->diffForHumans() }}</td>
          <td>
            <a href="/admin/user_edit/{{ $admin->id }}" class="btn btn-app">
                 <i class="fa fa-edit"></i> Edit
            </a>
            @if($admin->id != auth()->user()->id)
            <a class="btn btn-app delete">
                 <i class="fa fa-trash"></i> delete
            </a>
            @endif
          </td>
        </tr>
@endforeach
        </tbody>
      </table>
    </div>
  </div>
 <script>
function myFunction() {
    var x = document.getElementsByClassName("treeview");
    element.classList.add("active");
}
</script>
@endsection