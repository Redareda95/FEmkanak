<?php
    $car_id ="";
    $user_id ="";
if($order){

    $car_id =$order->car_id;
    $user_id =$order->user_id;
}
?>
@extends('../dashboard.master')
@section('content')
@if($flash = session('message'))

<div class="alert success">
  <span class="closebtn" onclick="this.parentElement.style.display='none';">&times;</span> 
    <b>{{ $flash }}</b>
</div>
@endif
@if($flash = session('deleted'))
<div class="alert">
  <span class="closebtn" onclick="this.parentElement.style.display='none';">&times;</span> 
    <b>{{ $flash }}</b>
</div>
@endif
<div class="box-header">
        <h3 class="box-title">Data Table For Site orders status</h3>
    </div>

<form role="form" method="post" action="{{url("admin/assign_order_action").'/'.$order_id}}">
    {!!csrf_field()!!}
    @if (count($errors) > 0)
        <div class="alert alert-danger">
            <strong>Whoops!</strong> There were some problems with your input.<br><br>
            <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
            </ul>
        </div>
    @endif
                  <div class="form-group">
                  <label for="exampleInputEmail1">Employes</label>
                  <?php 
//                     echo json_encode($employees);
                     ?>
                  <select  name="employee_id" class="form-control">
                      <option value="">--select-- </option>
                     
                      @foreach($employees as $employee )

                      <option value="{{$employee->id}}" @if($user_id == $employee->id){{"selected"}}@endif>{{$employee->first_name.' '.$employee->last_name}} </option>

                       @endforeach
                  </select>
                </div>
              
               <div class="form-group">
                  <label for="exampleInputEmail1">Car</label>
                  <select  name="car_id" class="form-control">
                      <option value="">--select-- </option>
                     
                      @foreach($cars as $car )
                      <option value="{{$car->id}}" @if($car_id == $car->id){{"selected"}}@endif>{{$car->name}} </option>
                       @endforeach
                  </select>
                </div>
              </div>

               
                    <div class="box-footer">
                <button type="submit" class="btn btn-primary">Submit</button>
                 <a  href='{{url("/admin/orders")}}' class="btn btn-default">Cancel</a>
              </div>
              
@if(isset($errorss))
@foreach($errorss as $err)
    <script>
    alert("{{ $err }}");
    </script>
@endforeach
@endif
            </form>

@endsection



