@extends('../dashboard.master')
@section('content')
@if($flash = session('message'))
<div class="alert success">
  <span class="closebtn" onclick="this.parentElement.style.display='none';">&times;</span> 
    <b>{{ $flash }}</b>
</div>
@endif
@if($flash = session('deleted'))
<div class="alert">
  <span class="closebtn" onclick="this.parentElement.style.display='none';">&times;</span> 
    <b>{{ $flash }}</b>
</div>
@endif
<div class="box-header">
        <h1 class="box-title">Data Table For Pending Orders</h1>
    </div>

<div class="row">
<!--<div class="col-md-2">
    <button type="button" class="btn btn-block btn-primary">Add Order +</button>
</div>-->
</div>
<div class="box">
      
    <div class="box-body">
      <table id="example1" class="table table-bordered table-striped" data-order='[[ 1, "asc" ]]' data-page-length='25'>
        <thead>
        <tr>
          <th>#</th>
          <th>Customer name</th>
          <th>Customer Phone #</th>
          <th>Service</th>
          <th>Date </th>
          <th>Time </th>
          <th>Status </th>
          <th>Ordered at</th>
          <th>Actions </th>
        </tr>
        </thead>
        <tbody>
@foreach($orders as $order)
        <tr table="orders" id="{{$order->id}}">
          <td>{{ $order->id }}</td>
          <td>{{$order->user->first_name}} {{$order->user->last_name}}</td>
          <td>{{ $order->user->phone }}</td>
          <td>{{$order->service_name}}</td>
          <td>{{ $order->date }}</td>
          @php
            $time = \App\AvailableDay::where('id', $order->time)->first();
          @endphp
          <td>{{ $time->from }}</td>
          <td><select onchange="getval(this,'{{$order->id}}');" id="select_status" class="form-control">
                  
                  <option value="Pending"@if($order->status=="Pending"){{"selected"}}@endif>Pending</option>
                  <option value="In progress"@if($order->status=="In progress"){{"selected"}}@endif>In progress</option>
                  <option value="Done"@if($order->status=="Done"){{"selected"}}@endif>Done</option>
                  <option value="Rejected"@if($order->status=="Rejected"){{"selected"}}@endif>Rejected</option>
              </select></td>
              <td>{{ $order->created_at->toFormattedDateString() }}</td>
         <td>
            <a href="{{url('/admin/edit_orders/').'/'.$order->id}}" class="btn btn-app">
                 <i class="fa fa-edit"></i> 
            </a>
            @if($order->status == "Pending")
              <a href="{{url('/admin/assign_order/').'/'.$order->id}}" class="btn btn-app">
                 <i class="fa fa-arrow-right"></i>
              </a>
            @endif
          </td>
        </tr>
@endforeach
        </tbody>
      </table>
    </div>
  </div>

@endsection