@extends('../dashboard.master')
@section('content')
@if($flash = session('message'))
<div class="alert success">
  <span class="closebtn" onclick="this.parentElement.style.display='none';">&times;</span> 
    <b>{{ $flash }}</b>
</div>
@endif
@if($flash = session('deleted'))
<div class="alert">
  <span class="closebtn" onclick="this.parentElement.style.display='none';">&times;</span> 
    <b>{{ $flash }}</b>
</div>
@endif
<div class="box-header">
        <h3 class="box-title">Data Table For Site orders status</h3>
    </div>

<form role="form" method="post" action="{{url("admin/update_orders").'/'.$order->id}}">
    {!!csrf_field()!!}
              <div class="box-body">
                <div class="form-group">
                  <label for="exampleInputEmail1"><i class="fa fa-user"></i> Client Full Name:- {{ $order->user->first_name }} {{ $order->user->last_name }}</label>
                </div>
                <div class="form-group">
                  <label for="exampleInputEmail1"><i class="fa fa-phone"></i> Client Phone #:- {{ $order->user->phone }}</label>
                </div>
                <div class="form-group">
                  <label for="exampleInputEmail1"><i class="fa fa-at"></i> Client Email Address:- {{ $order->user->email }}</label>
                </div>
                <div class="form-group">
                  <label for="exampleInputEmail1"><i class="fa fa-upload"></i> Ordered At :- {{ $order->created_at }}</label>
                </div>
              <div class="form-group">
                  <label>Time:</label>
                  <div class="input-group">
                    <select name="time" class="form-control">
                      @foreach($days as $day)
                        <option value="{{ $day->id }}" @if($day->id == $order->time) {{ 'selected' }} @endif>{{ $day->from }}</option>
                      @endforeach
                    </select>
                    <div class="input-group-addon">
                      <i class="fa fa-clock-o"></i>
                    </div>
                  </div>
                  <!-- /.input group -->
                </div>
                  
               <div class="form-group">
                <label>Date:</label>

                <div class="input-group date">
                  <div class="input-group-addon">
                    <i class="fa fa-calendar"></i>
                  </div>
                    <input type="text" value="{{$order->date}}" name="date"class="form-control pull-right" id="datepicker">
                </div>
                <!-- /.input group -->
              </div>
                  
                <div id="mapReda" style="width:100%;height:500px"></div>
    
              <!-- /.box-body -->

              <div class="box-footer">
                <button type="submit" class="btn btn-primary">Submit</button>
                <a href="{{url("/admin/orders")}}"> <button  class="btn btn-default">Cancel</button></a>
              </div>
            </form>

<script>
 function initMap() {
  var uluru = {lat: {{$order->lat}}, lng: {{$order->long}}};
  var map = new google.maps.Map(document.getElementById('mapReda'), {
    zoom: 17,
    center: uluru
  });
  var geocoder = new google.maps.Geocoder();

  var contentString = '<span class="label label-link"><mark>Latitude:-  {{ $order->lat }}</mark></span><br><span class="label label-link"><mark>Longitude:- {{ $order->long }} </mark></span>';

  var infowindow = new google.maps.InfoWindow({
    content: contentString
  });

  var marker = new google.maps.Marker({
    position: uluru,
    map: map,
    title: 'Uluru (Ayers Rock)'
  });
  marker.addListener('click', function() {
    infowindow.open(map, marker);
  });
}

</script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyA3Yj4XQ1rNfsgb1heGuRW95ZPLrSs75lo&callback=initMap"
    async defer></script>
@endsection