@extends('dashboard.master')
@section('content')

<form method="POST" action="/admin/restrict_day">
    {{ csrf_field() }}

            <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Restrict a Date & Time</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
              <div class="box-body">
                <div class="col-md-6">
                <div class="form-group">
                  <label for="exampleInputEmail1">Choose Date: </label>
                  <input type="date" class="form-control" id="dateCh" min="{{ $date }}" name="day">
                </div>
              </div>
                <div class="col-md-6">
                <div class="form-group" style="display: none;" id="timeDisplay">
                  <label>City</label>
                  <select class="form-control" id="timeo" name="available_day_id">
                    <option disabled>Choose Date</option>
                  </select>
                </div>
              </div>
              </div>
          </div>
<div class="box-footer">
<button type="submit" class="btn btn-danger btn-block btn-flat">Restrict</button>
</div>
</form>
@if(count($errors))
<div class="alert alert-danger">
<ul>
    @foreach($errors->all() as $error)
        <li>{{ $error }}</li>
    @endforeach
</ul>
</div>
@endif
@endsection