@extends('dashboard.master')
@section('content')
@if($flash = session('message'))
<div class="alert success">
  <span class="closebtn" onclick="this.parentElement.style.display='none';">&times;</span> 
    <b>{{ $flash }}</b>
</div>
@endif
@if($flash = session('deleted'))
<div class="alert">
  <span class="closebtn" onclick="this.parentElement.style.display='none';">&times;</span> 
    <b>{{ $flash }}</b>
</div>
@endif
@php
  $i=1;
@endphp
<div class="box">
  <div class="box-header">
  <center><h3 class="box-title">All Available Working Days</h3></center>
            <a href="/admin/restrict_day" class="btn btn-danger btn-lg pull-left" title="To block a certain time & day to be previewed in booking form">
              <i class="fa fa-ban"></i> Restrict Timing
            </a>
</div>
    <div class="box-body">
      <table id="example1" class="table table-bordered table-striped" data-page-length='25'>
        <thead>
        <tr>
          <th>#</th>
          <th>From</th>
          <th>To</th>
          <th colspan="2">Edit</th>
        </tr>
        </thead>
        <tbody>
@foreach($days as $day)
        <tr table="cars" id="{{$day->id}}">
          <td>{{ $i++ }}</td>
          <form method="POST" action="/admin/available_day_edit/{{ $day->id }}">
            {{ csrf_field() }}
            <input type="hidden" name="_method" value="PATCH">
            <td>
              <div class="col-xs-5">
                  <input type="text" class="form-control" value="{{ $day->from }}" name="from">
                </div>
              </td>
            <td>
              <div class="col-xs-5">
                  <input type="text" class="form-control" value="{{ $day->to }}" name="to">
                </div>
              </td>            
              <td>
              <button class="btn btn-app" type="submit">
                   <i class="fa fa-edit"></i> Change working time?
              </button>
            </td>
            @if(count($errors))
            <td>
              <div class="alert alert-danger">
              <ul>
                  @foreach($errors->all() as $error)
                      <li>{{ $error }}</li>
                  @endforeach
              </ul>
              </div>
            </td>
            @endif
        </form>
        </tr>
@endforeach
        </tbody>
      </table>
    </div>
  </div>
@endsection