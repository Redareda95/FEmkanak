@extends('dashboard.master')
@section('content')
<style>
    .exportExcel{
  padding: 5px;
  border: 1px solid grey;
  margin: 5px;
  cursor: pointer;
}
</style>
@if($flash = session('message'))
<div class="alert success">
  <span class="closebtn" onclick="this.parentElement.style.display='none';">&times;</span> 
    <b>{{ $flash }}</b>
</div>
@endif
@if($flash = session('deleted'))
<div class="alert">
  <span class="closebtn" onclick="this.parentElement.style.display='none';">&times;</span> 
    <b>{{ $flash }}</b>
</div>
@endif

<div class="box">
            <div class="box-header">
  <h3 class="box-title">Data Table For Employees</h3>
</div>
    <div class="box-body">
      <table id="example1" class="table table-bordered table-striped">
        <thead>
        <tr>
          <th>ID</th>
          <th>First Name</th>
          <th>Last Name</th>
          <th>Email</th>
          <th>Phone #</th>
          <th>Date of Creation</th>
          <th>Edit / Delete</th>
        </tr>
        </thead>
        <tbody>
@foreach($employees as $employee)
        <tr table="users" id="{{$employee->id}}">
          <td>{{ $employee->id }}</td>
          <td><a href="{{url('/admin/users/emp_history').'/'.$employee->id}}">{{ $employee->first_name }}</a></td>
          <td>{{ $employee->last_name }}</td>
          <td>{{ $employee->email }}</td>
          <td>{{ $employee->phone }}</td>
          <td>{{ $employee->created_at->toFormattedDateString() }}</td>
          <td>
            <a href="/admin/employee_edit/{{ $employee->id }}" class="btn btn-app">
                 <i class="fa fa-edit"></i> Edit
            </a>
              <a class="btn btn-app delete">
                 <i class="fa fa-trash"></i> delete
            </a>
          </td>
        </tr>
@endforeach
        </tbody>
      </table>
    </div>
  </div>

@endsection