  <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">
          <img src="/dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
          <p>{{ Auth::user()->first_name }}</p>
          <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
        </div>
      </div>
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu" data-widget="tree">
        <li class="header">MAIN NAVIGATION</li>
        @if(Auth::user()->hasRole('admin'))
        <li class="treeview">
          <a href="#">
            <i class="fa fa-users"></i> <span>Users</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="/users"><i class="fa fa-circle-o"></i>Users & Roles</a></li>
            <li><a href="/admin/all-users"><i class="fa fa-circle-o"></i>Registered Users</a></li>
            <li><a href="/admin/admins"><i class="fa fa-circle-o"></i>Admins</a></li>
          </ul>
        </li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-cogs"></i>
            <span>Services</span>
            <span class="pull-right-container">
              <span class="label label-primary pull-right">{{ \App\Service::count() }}</span>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="/admin/service"><i class="fa fa-circle-o"></i> All Services</a></li>
            <li><a href="/admin/service_create"><i class="fa fa-circle-o"></i> Add Services</a></li>
          </ul>
        </li>
         <li class="treeview">
          <a href="#">
            <i class="fa fa-car"></i>
            <span>Cars</span>
            <span class="pull-right-container">
              <span class="label label-primary pull-right">{{ \App\Car::count() }}</span>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="/admin/car"><i class="fa fa-circle-o"></i> All Cars</a></li>
            <li><a href="/admin/car_create"><i class="fa fa-circle-o"></i> Add Car</a></li>
          </ul>
        </li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-wrench"></i>
            <span>Employee</span>
            <span class="pull-right-container">
              <span class="label label-primary pull-right">{{ \App\User::where('user_type', 'employee')->count() }}</span>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="/admin/employee"><i class="fa fa-circle-o"></i> All Employees</a></li>
            <li><a href="/admin/employee_create"><i class="fa fa-circle-o"></i> Add Employee</a></li>
          </ul>
        </li>
@endif
        <li class="treeview">
          <a href="{{url("admin/orders")}}">
            <i class="fa fa-files-o"></i>
            <span>Orders</span>
            <span class="pull-right-container">
              <span class="label label-primary pull-right">{{ \App\Order::count() }}</span>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="/admin/orders"><i class="fa fa-circle-o"></i> All Orders</a></li>
            <li><a href="/admin/pending_orders"><i class="fa fa-circle-o"></i> Pending Orders</a></li>
            <li><a href="/admin/inprogress_orders"><i class="fa fa-circle-o"></i> Inprogress Orders</a></li>
            <li><a href="/admin/done_orders"><i class="fa fa-circle-o"></i>Done Orders</a></li>
            <li><a href="/admin/rejected_orders"><i class="fa fa-circle-o"></i> Rejected Orders</a></li>
            <!--<li><a href="/admin/add_orders"><i class="fa fa-circle-o"></i> Add Order</a></li>-->
          </ul>
        </li>     
@if(Auth::user()->hasRole('admin'))
          <li>
            <a href="/admin/available_days">
            <i class="fa fa-calendar"></i><span>Available Days</span>
            </a>
          </li>
          <li>
            <a href="/admin/site_settings">
            <i class="fa fa-info"></i> <span>View & Edit Site Settings</span>
            </a>
          </li>
          <li>
            <a href="/admin/public_texts">
            <i class="fa fa-plus"></i> <span>Home Page Texts</span>
            </a>
          </li>
        <li>
          <a href="/admin/mails">
            <i class="fa fa-envelope"></i> <span>Messages From Contact Us</span>
            <span class="pull-right-container">
              <small class="label pull-right bg-yellow"></small>
              <small class="label pull-right bg-green"></small>
              <small class="label pull-right bg-red"></small>
            </span>
          </a>
        </li>
        <li class="header">Other Options</li>
<!--         <li><a href="/password/reset"><i class="fa fa-circle-o text-red"></i> <span>Change Admin Password</span></a></li>
 -->        <li><a href="/"><i class="fa fa-circle-o text-yellow"></i> <span>View Site</span></a></li>
        <li><a href="/admin/register"><i class="fa fa-circle-o text-aqua"></i> <span>Add an admin</span></a></li>
@endif
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>
  <div class="content-wrapper">

