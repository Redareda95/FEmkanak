@extends('../dashboard.master')
@section('content')
@if($flash = session('message'))
<div class="alert success">
  <span class="closebtn" onclick="this.parentElement.style.display='none';">&times;</span> 
    <b>{{ $flash }}</b>
</div>
@endif
@if($flash = session('deleted'))
<div class="alert">
  <span class="closebtn" onclick="this.parentElement.style.display='none';">&times;</span> 
    <b>{{ $flash }}</b>
</div>
@endif
<div class="box-header">
        <h1 class="box-title">Data Table For <a href="{{url('/admin/car_edit').'/'.$car->id}}">{{$car->name}} </a>Orders</h1>
    </div>
<div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-green"><i class="fa fa-car"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">Done Orders</span>
              <span class="info-box-number">{{$count_Done_order}}</span>
            </div>
          </div>
        </div>
        
<div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-red"><i class="fa fa-car"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">Rejected Orders</span>
              <span class="info-box-number">{{$count_rej_order}}</span>
            </div>
          </div>
        </div>
        
<div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-yellow"><i class="fa fa-car"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">In progress Orders</span>
              <span class="info-box-number">{{$count_Inprogress_order}}</span>
            </div>
          </div>
        </div>
        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-blue"><i class="fa fa-car"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">Pending Orders</span>
              <span class="info-box-number">{{$count_pending_order}}</span>
            </div>
          </div>
        </div>
<div class="row">
<!--<div class="col-md-2">
    <button type="button" class="btn btn-block btn-primary">Add Order +</button>
</div>-->
</div>


<div><div>
<div class="box">
      
    <div class="box-body">
      <table id="example1" class="table table-bordered table-striped" data-order='[[ 1, "asc" ]]' data-page-length='25'>
        <thead>
        <tr>
          <th>#</th>
          <th>Service</th>
          <th>Date </th>
          <th>Time </th>
          <th>Longitude </th>
          <th>Latitude </th>
          <th>Status </th>
          <th>Ordered at</th>
          <th>Actions </th>
        </tr>
        </thead>
        <tbody>
@foreach($array_of_orders as $order)
        <tr table="orders" id="{{$order->id}}">
          <td>{{ $order->id }}</td>
          <td>@if($order->service_name){{ $order->service_name }}@endif</td>
          <td>{{ $order->date }}</td>
          @php
            $time = \App\AvailableDay::where('id', $order->time)->first();
          @endphp
          <td>{{ $time->from }}</td>
          <td>{{ $order->long }}</td>
          <td>{{ $order->lat }}</td>
          <td>{{$order->status}}</td>
              <td>{{ $order->created_at->toFormattedDateString() }}</td>
         <td>
            <a href="{{url('/admin/edit_orders/').'/'.$order->id}}" class="btn btn-app">
                 <i class="fa fa-edit"></i> 
            </a>
            @if($order->status == "Pending")
              <a href="{{url('/admin/assign_order/').'/'.$order->id}}" class="btn btn-app">
                 <i class="fa fa-arrow-right"></i>
              </a>
            @endif
          </td>
        </tr>
@endforeach
        </tbody>
      </table>
    </div>
  </div>

@endsection