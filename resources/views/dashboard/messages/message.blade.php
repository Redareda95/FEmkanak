@extends('dashboard.master')
@section('content')
<div class="col-md-3">
          <div class="box box-solid">
            <div class="box-header with-border">
              <h3 class="box-title">Folders</h3>

              <div class="box-tools">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
              </div>
            </div>
            <div class="box-body no-padding" style="">
              <ul class="nav nav-pills nav-stacked">
                <li><a href="/admin/mails"><i class="fa fa-inbox"></i> Inbox
                  <span class="label label-primary pull-right">{{ \App\ContactForm::count() }}</span></a></li>
              </ul>
            </div>
          </div>
        </div>
<div class="col-md-9">
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Read Mail</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body no-padding">
              <div class="mailbox-read-info">
                <h3>{{ $message->subject }}</h3>
                <h5>From: {{ $message->email }}
                  <span class="mailbox-read-time pull-right">{{ $message->created_at->toFormattedDateString() }}</span></h5>
              </div>
              <div class="mailbox-read-message">
                <?= $message->message ?>
              </div>
              <!-- /.mailbox-read-message -->
            </div>
            <div class="box-footer contact_forms{{$message->id}}" table="contact_forms" id="{{$message->id}}">
              <a class="delete"><button type="button" class="btn btn-default"><i class="fa fa-trash-o"></i> Delete</button></a>
              <button type="button" onclick="myFunction()" class="btn btn-default"><i class="fa fa-print"></i> Print</button>
            </div>
            <!-- /.box-footer -->
          </div>
          <!-- /. box -->
        </div>

<script>
function myFunction() {
    window.print();
}
</script>        
              @endsection