@extends('dashboard.master')
@section('content')
            <div class="row">
              <div class="col-md-3">
          <div class="box box-solid">
            <div class="box-header with-border">
              <h3 class="box-title">Folders</h3>

              <div class="box-tools">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
              </div>
            </div>
            <div class="box-body no-padding" style="">
              <ul class="nav nav-pills nav-stacked">
                <li><a href="/admin/mails"><i class="fa fa-inbox"></i> Inbox
                  <span class="label label-primary pull-right">{{ count($messages) }}</span></a></li>
              </ul>
            </div>
          </div>
        </div>
               <div class="col-md-9">
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Inbox</h3>

              <div class="box-tools pull-right">
                <div class="has-feedback">
                </div>
              </div>
              <div class="box-body no-padding">
              <div class="mailbox-controls">
                <!-- Check all button -->
                <!-- /.btn-group -->
                <button type="button" class="btn btn-default btn-sm"><i class="fa fa-refresh"></i></button>
                <div class="pull-right">
                  {{ count($messages) }}
                  <div class="btn-group">
                    <button type="button" class="btn btn-default btn-sm"><i class="fa fa-chevron-left"></i></button>
                    <button type="button" class="btn btn-default btn-sm"><i class="fa fa-chevron-right"></i></button>
                  </div>
                  <!-- /.btn-group -->
                </div>
                <!-- /.pull-right -->
              </div>
              <div class="table-responsive mailbox-messages">
                <table class="table table-hover table-striped">
                  <tbody>
                  @foreach($messages as $m)
                  <tr>
                    <td><div class="icheckbox_flat-blue" aria-checked="false" aria-disabled="false" style="position: relative;"><input type="checkbox" style="position: absolute; opacity: 0;"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;"></ins></div></td>
                    <td class="mailbox-star"><a href="/admin/mail/{{ $m->id }}">
                      @if($m->read === 'un_read')<i class="fa fa-eye text-yellow"></i>@endif</a></td>
                    <td class="mailbox-name"><a href="/admin/mail/{{ $m->id }}">{{ $m->name }}</a></td>
                    <td class="mailbox-subject"><b>{{ $m->subject }}</b> - <?= substr($m->message,0, 100) . '...'; ?>
                    </td>
                    <td class="mailbox-attachment"></td>
                    <td class="mailbox-date">{{ $m->created_at->diffForHumans() }}</td>
                  </tr>
                  @endforeach
                  </tbody>
                </table>
                <!-- /.table -->
              </div>
              <!-- /.mail-box-messages -->
            </div>
      @endsection