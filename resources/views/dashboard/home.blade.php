@extends('dashboard.master')
@section('content')
<?php 

use App\Http\Controllers\commonController;
?>
<div class="row">
        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-aqua"><i class="ion ion-ios-gear-outline"></i></span>
            <div class="info-box-content">
              <span class="info-box-text">All Services</span>
              <span class="info-box-number">{{ \App\Service::count() }}<small> Different Services</small></span>
            </div>
          </div>
        </div>
        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-red"><i class="fa fa-car"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">Cars</span>
              <span class="info-box-number">{{ \App\Car::count() }}</span>
            </div>
          </div>
        </div>
        <div class="clearfix visible-sm-block"></div>
        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-green"><i class="fa fa-wrench"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">Employee</span>
              <span class="info-box-number">{{ \App\User::where('user_type', 'employee')->count() }}</span>
            </div>
          </div>
        </div>
        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-yellow"><i class="ion ion-ios-people-outline"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">New Members</span>
              <span class="info-box-number">{{ \App\User::where('user_type', 'user')->count() }}</span>
            </div>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-lg-4 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-aqua">
            <div class="inner">
              <h3>{{ \App\Order::where('status', 'Pending')->count() }}</h3>

              <p>New Orders</p>
            </div>
            <div class="icon">
              <i class="ion ion-bag"></i>
            </div>
            <a href="/admin/pending_orders" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <div class="col-lg-4 col-xs-6">
          <div class="small-box bg-aqua">
                <div class="inner">
                  <h3>{{ \App\Order::where('status', 'In progress')->count() }}</h3>

                  <p>In Progress Orders</p>
                </div>
                <div class="icon">
                  <i class="fa fa-shopping-cart"></i>
                </div>
                <a href="/admin/inprogress_orders" class="small-box-footer">
                  More info <i class="fa fa-arrow-circle-right"></i>
                </a>
              </div>
        </div>
        <div class="col-lg-4 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-green">
            <div class="inner">
              <h3>{{ \App\Order::count() }}<sup style="font-size: 20px"></sup></h3>

              <p>Total Orders</p>
            </div>
            <div class="icon">
              <i class="ion ion-stats-bars"></i>
            </div>
            <a href="/admin/orders" class="small-box-footer">
              More info <i class="fa fa-arrow-circle-right"></i>
            </a>
          </div>
        </div>

        </div>
<div class="row">
<div class="col-md-6">
<div class="box box-info">
            <div class="box-header with-border">
              <h3 class="box-title">Latest Orders</h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              </div>
            </div>
            <div class="box-body">
              <div class="table-responsive">
                <table class="table no-margin">
                  <thead>
                  <tr>
                    <th>Order ID</th>
                    <th>User Name</th>
                    <th>Status</th>
                    <th>Ordered At</th>
                  </tr>
                  </thead>
                  <tbody>
                  	@foreach($orders as $order)
                  <tr>
                    <td><a href="#">{{ $order->id }}</a></td>
                    <td>{{ $order->user->first_name }}</td>
                    <td>
                    	@if($order->status == 'Done')
                    	<span class="label label-success">Done</span></td>
                    	@elseif($order->status == 'In progress')
                    	<span class="label label-info">In Progress</span></td>
                    	@elseif($order->status == 'Rejected')
                    	<span class="label label-danger">Rejected</span></td>
                    	@else
                    	<span class="label label-warning">Pending</span></td>
                    	@endif
                    <td>
                       <span>{{ $order->created_at }}</span>
                    </td>
                  </tr>
                  @endforeach
                  </tbody>
                </table>
              </div>
            </div>
            <div class="box-footer clearfix">
              <a href="/admin/inprogress_orders" class="btn btn-sm btn-info btn-flat pull-left">Assigned Orders</a>
              <a href="/admin/orders" class="btn btn-sm btn-default btn-flat pull-right">View All Orders</a>
            </div>
          </div>
      </div>
      
      
      
      <div class="col-md-6">
        <div class="box box-warning">
                <div class="box-header with-border">
                  <p class="text-center">
                    <strong>Today Order Statistics</strong>
                  </p>
                  <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                    </button>
                    <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i>
                    </button>
                  </div>
                </div>
                <div class="box-body">

                  <div class="progress-group">
                    <span class="progress-text">Requested orders</span>
                    <span class="progress-number"><b>{{  commonController::today_requests() }}</b>/{{ commonController::today_orders() }}</span>
                    @php
                      $percentage_c = (commonController::today_requests()/commonController::today_orders())* 100;
                    @endphp
                    <div class="progress sm">
                      <div class="progress-bar progress-bar-red" style="width: {{ $percentage_c }}%"></div>
                    </div>
                  </div>
                  
                  
                </div>
              </div>
            </div>
            
            
      @if($all_orders_count != 0)
      <div class="col-md-6">
        <div class="box box-danger">
                <div class="box-header with-border">
                  <p class="text-center">
                    <strong>Orders Statistics</strong>
                  </p>
                  <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                    </button>
                    <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i>
                    </button>
                  </div>
                </div>
                <div class="box-body">

                  <div class="progress-group">
                    <span class="progress-text">Successfull Orders</span>
                    <span class="progress-number"><b>{{ $done_orders_count }}</b>/{{ $all_orders_count }}</span>
                    @php
                      $percentage = ($done_orders_count/$all_orders_count)* 100;
                    @endphp
                    <div class="progress sm">
                      <div class="progress-bar progress-bar-green" style="width: {{ $percentage }}%"></div>
                    </div>
                  </div>
                  <div class="progress-group">
                    <span class="progress-text">Inprogress Orders</span>
                    <span class="progress-number"><b>{{ $inprog_orders_count }}</b>/{{ $all_orders_count }}</span>
                    @php
                      $percentage_i = ($inprog_orders_count/$all_orders_count)* 100;
                    @endphp
                    <div class="progress sm">
                      <div class="progress-bar progress-bar-aqua" style="width: {{ $percentage_i }}%"></div>
                    </div>
                  </div>
                  <div class="progress-group">
                    <span class="progress-text">Cancelled Orders</span>
                    <span class="progress-number"><b>{{ $cancelled_orders_count }}</b>/{{ $all_orders_count }}</span>
                    @php
                      $percentage_c = ($cancelled_orders_count/$all_orders_count)* 100;
                    @endphp
                    <div class="progress sm">
                      <div class="progress-bar progress-bar-red" style="width: {{ $percentage_c }}%"></div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            @endif
  </div>
<div class="row">
	<div class="col-md-6">
		<div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Cars in Service</h3>
              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              </div>
            </div>
            <div class="box-body">
              <ul class="products-list product-list-in-box">
              	@foreach($cars as $car)
                <li class="item">
                  <div class="product-img">
                    <img src="/dist/img/default-50x50.gif" alt="Product Image">
                  </div>
                  <div class="product-info">
                    <a href="javascript:void(0)" class="product-title">{{ $car->name }}
                  @if($car->status == 1)
                      <span class="label label-success pull-right">Available</span>
                  @else
				      <span class="label label-danger pull-right">Un-Available</span>
				          @endif
              		</a>
                    <span class="product-description">
                          <?= $car->desc; ?>
                    </span>
                  </div>
                </li>
                @endforeach
              </ul>
            </div>
            <div class="box-footer text-center">
              <a href="/admin/car" class="uppercase">View Cars</a>
            </div>
          </div>
	</div>
  <div class="col-md-6">
      <div class="box box-danger">
                <div class="box-header with-border">
                  <h3 class="box-title">Latest Members</h3>

                  <div class="box-tools pull-right">
                    <span class="label label-danger">New Members</span>
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                    </button>
                    <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i>
                    </button>
                  </div>
                </div>
                <div class="box-body no-padding">
                  <ul class="users-list clearfix">
                    @foreach($users as $user)
                    <li>
                      @if($user->image)
                        <img src="{{$user->image}}" alt="User Image" width="85">
                      @else
                        <img src="/uploads/profile.jpg" alt="User Image" width="85">
                      @endif
                      <a class="users-list-name" href="#">{{ $user->first_name }} {{ $user->last_name }}</a>
                      <span class="users-list-date">{{ $user->created_at->diffForHumans() }}</span>
                    </li>
                    @endforeach
                  </ul>
                </div>
                <div class="box-footer text-center">
                  <a href="/admin/all-users" class="uppercase">View All Users</a>
                </div>
              </div>
      </div>
</div>
@endsection