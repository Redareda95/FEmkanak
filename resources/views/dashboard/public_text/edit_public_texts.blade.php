@extends('../dashboard.master')
@section('content')
@if($flash = session('message'))
<div class="alert success">
  <span class="closebtn" onclick="this.parentElement.style.display='none';">&times;</span> 
    <b>{{ $flash }}</b>
</div>
@endif
@if($flash = session('deleted'))
<div class="alert">
  <span class="closebtn" onclick="this.parentElement.style.display='none';">&times;</span> 
    <b>{{ $flash }}</b>
</div>
@endif
<?php 
use App\Http\Controllers\commonController;
?>
<div class="box-header">
        <h3 class="box-title">Data Table For Site public_texts status</h3>
    </div>

<form role="form" method="post" action="{{url("admin/public_texts_update").'/'.$public_text->id}}">
    {!!csrf_field()!!}
              <div class="box-body">
              
                <div class="form-group">
                  <label for="exampleInputEmail1">Name</label>
                  <input type="status" value="{{$public_text->name}}" name="value" disabled=""class="form-control" id="exampleInputEmail1" placeholder="Enter email">
                </div>
                <div class="form-group">
                  <label for="exampleInputEmail1">English Value</label>
                  <input type="status" value="{{commonController::englishTranslate($public_text->value)}}" name="value"class="form-control" id="exampleInputEmail1" placeholder="Enter email">
                </div>
                  
                <div class="form-group">
                  <label for="exampleInputEmail1">Arabic Value</label>
                  <input type="status" value="{{commonController::arabicTranslate($public_text->value)}}" name="arabicvalue"class="form-control" id="exampleInputEmail1" placeholder="Enter email">
                </div>
              
    
              <!-- /.box-body -->

              <div class="box-footer">
                <button type="submit" class="btn btn-primary">Submit</button>
                <a href="{{url("/admin/public_texts")}}"class="btn btn-default"> Cancel</a>
              </div>
            </form>
</div>
@endsection