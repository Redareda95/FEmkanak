@extends('../dashboard.master')
@section('content')
@if($flash = session('message'))

<div class="alert success">
  <span class="closebtn" onclick="this.parentElement.style.display='none';">&times;</span> 
    <b>{{ $flash }}</b>
</div>
@endif
@if($flash = session('deleted'))
<div class="alert">
  <span class="closebtn" onclick="this.parentElement.style.display='none';">&times;</span> 
    <b>{{ $flash }}</b>
</div>
@endif
<?php 
use App\Http\Controllers\commonController;
?>
<div class="box-header">
        <h1 class="box-title">Data Table For All public text in web site</h1>
    </div>

<div class="row">
<!--<div class="col-md-2">
    <button type="button" class="btn btn-block btn-primary">Add public_text +</button>
</div>-->
</div>
<div class="box">
      
    <div class="box-body">
      <table id="example1" class="table table-bpublic_texted table-striped">
        <thead>
        <tr>
          <th>#</th>
          <th>Text name</th>
          <th>Text Value</th>
          <th>Arabic Value</th>
          <th>Actions</th>
        </tr>
        </thead>
        <tbody>
@foreach($public_texts as $public_text)
        <tr table="public_text" id="{{$public_text->id}}">
          <td>{{ $public_text->id }}</td>
          <td> {{ $public_text->name }}</td>
          <td>{{ commonController::englishTranslate($public_text->value) }}</td>
          <td>{{ commonController::arabicTranslate($public_text->value) }}</td>
          
         <td>
            <a href="{{url('/admin/public_texts_edit/').'/'.$public_text->id}}" class="btn btn-app">
                 <i class="fa fa-edit"></i> 
            </a>
           
          </td>
        </tr>
@endforeach
        </tbody>
      </table>
    </div>
  </div>

@endsection