@extends('master')
@section('content')
@php
  $lang = (isset($_GET['lang'])) ? $_GET['lang'] : 'en' ;
@endphp 
<?php 
use App\Http\Controllers\commonController;
?>
 <style>
            
body{
        background:#000;
}
.login-wrap{
    width:100%;
    margin:auto;
    max-width:700px;
    position:relative;
    background:url(/images/22.jpg) no-repeat center;
    box-shadow:0 12px 15px 0 rgba(225,0,0,.24),0 17px 50px 0 rgba(225,0,0,.19);
}
.mytable>tbody>tr>td, .mytable>tbody>tr>th, .mytable>tfoot>tr>td, .mytable>tfoot>tr>th, .mytable>thead>tr>td, .mytable>thead>tr>th {
    padding: 12px;
}

    </style>
<!------ Contact Wrapper Start ------>
    <div class="impl_contact_wrapper ">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12" style="text-align: center; padding-bottom:20px">
                    <h1 >@if($lang == 'en'){{'Your Requests'}} @else {{ 'طلباتك السابقة' }} @endif</h1>
                </div>
                
<div class="col-lg-12 col-md-12">
<div class="login-wrap">
         <table class="table mytable" style="color:#fff">
             <thead class="thead-dark">
                 <tr >
                     <th>@if($lang == 'en'){{'Order ID'}} @else {{'رقم الطلبية'}}@endif</th>
                     <th>@if($lang == 'en'){{'Order Date & Time'}} @else {{'ميعاد الطلبية'}} @endif</th>
                     <th>@if($lang == 'en'){{ 'Order Status' }} @else {{"حالة الطلبية"}} @endif</th>
                     <th>@if($lang == 'en'){{'Ordered At'}} @else {{'تاريخ الحجز'}} @endif</th>
                 </tr>
             </thead>
             <tbody>
                 @foreach($previous_orders as $order)
                <tr class=@if($order->status == "In progress"){{"table-warning"}}@elseif($order->status == "Done"){{"table-info"}}@elseif($order->status == "Rejected"){{"table-danger"}}@else {{""}}@endif style=@if($order->status == "In progress"){{'color:black'}}@elseif($order->status == "Done"){{"color:black"}}@elseif($order->status == "Rejected"){{"color:black"}}@else {{"color:#fff"}}@endif>
                  <td>{{ $order->id }}</td>
                  @php
                    $time = \App\AvailableDay::where('id', $order->time)->first();
                  @endphp
                  <td>{{ $order->date }} / {{ $time->from }}</td>
                  <td>{{ $order->status }}</td>
                  <td>{{ $order->created_at->toFormattedDateString() }}</td>
                </tr>
                @endforeach
            </tbody>
         </table>  
         	<div>{{ $previous_orders->render() }}</div>

</div>
</div>
            </div>
 </div>
            </div>
    </div>
@endsection

