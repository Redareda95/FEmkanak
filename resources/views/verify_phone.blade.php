@extends('master')
@section('content')
@php
  $lang = (isset($_GET['lang'])) ? $_GET['lang'] : 'en' ;
@endphp 
<?php 
use App\Http\Controllers\commonController;
?>
<!-- Form -->
    <!------ Sell wrapper  Start ------>
    <div class="impl_sell_wrapper one" style="height: 1250px !important">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12" style="text-align: center">
					<h1>@if($lang == 'en'){{'Verify Number, to get our Service'}} @else {{'التحقق من الهاتف'}}  @endif </h1>
                </div>
                <div class="col-lg-12 col-md-12">
                   <div id="overlay"></div>
	<form action="/verfiy_phone" class="container booking" name="booking" method="POST">
	{{ csrf_field() }}
<br><br>
  <div id="verify_true" class="sent_success"><center><p>@if($lang == 'en') Successfully vertified @else تم التحقق بنجاح @endif<p></center></div>
                   
                   <div id="verify_false" class="sent_fail"><center><p>@if($lang == 'en')Pin code invalid @else الرمز خطأ  @endif<p></center></div>
                   <br><br>
         @if(count($errors))
          @foreach($errors->all() as $error)
          <div class="alert-danger">     {{ $error }};</div>
          @endforeach
        @endif
		
	
           <div class="persons">

            <label>@if($lang == 'en')phone @else الهاتف@endif</label>
            <div class="input-text ">
                <input type="text" id="phone" placeholder="" value="{{Auth::user()->phone}}" name="phone"> 
            </div>
                   </div>
                   <br><br>
                   <div id="check_true" class="sent_success"><center><p>@if($lang == 'en')Sent Successfully @else تم الإرسال بنجاح  @endif<p></center></div>
                   
                   <div id="check_false" class="sent_fail"><center><p>@if($lang == 'en')Fail Try Again <br> enter valid number @else  حدث خطأ في الإر سال <br>تأكد من الرقم  @endif<p></center></div>
        <br><br> <center><button id="send_code" class="impl_btn">@if($lang == 'en') send pin  @else  إرسال الرمز  @endif</button></center>
           
        <input id="user_obj" value="{{Auth::user()->id}}" hidden="">
        <div class="persons">
             <label>@if($lang == 'en')pin code @else الرمز المرسل@endif</label>
            <div class="input-text">
               <input type="text" name="pin_code" id="pin_code" placeholder="">
            </div>
        </div>

		
                 
      <br><br>
		 <div class="form-group">
            <center>
                <button id="verify_number" class="impl_btn">@if($lang == 'en')Verify @else  تحقق  @endif</button>
            </center>
         </div>
	</form>
                </div>
            </div>
        </div>
    </div>
@endsection
