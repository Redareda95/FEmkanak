@php
  $lang = (isset($_GET['lang'])) ? $_GET['lang'] : 'en' ;
  $settings = \App\SiteSetting::find(1);
@endphp 
<!DOCTYPE html>
<?php 
use App\Http\Controllers\commonController;
?>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->
<!-- Begin Head -->

<head>
    <title>{{ $settings->site_name }}</title>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1.0" name="viewport">
    <meta name="description" content="{{ $settings->description }}">
    <meta name="keywords" content="{{ $settings->keywords }}">
    <meta name="author" content="{{ $settings->author }}">
    <meta name="MobileOptimized" content="320">
    <!--Srart Style -->
    <link rel="stylesheet" type="text/css" href="\css\fonts.css">
  <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="stylesheet" type="text/css" href="\css\bootstrap.min.css">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

        
    <link rel="stylesheet" type="text/css" href="\css\font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="\css\ion.rangeSlider.css">
    <link rel="stylesheet" type="text/css" href="\css\ion.rangeSlider.skinFlat.css">
    <!--<link rel="stylesheet" type="text/css" href="\css\normalize.css">-->
    <link rel="stylesheet" type="text/css" href="\css\datepicker.css">
    <link rel="stylesheet" type="text/css" href="\js\plugin\magnific\magnific-popup.css">
    <link rel="stylesheet" type="text/css" href="\js\plugin\slick\slick.css">
    <link rel="stylesheet" type="text/css" href="\js\plugin\slick\slick-theme.css">
    <link rel="stylesheet" type="text/css" href="\js\plugin\nice_select\nice-select.css">
     <link href="https://fonts.googleapis.com/css?family=Lobster" rel="stylesheet">
    
<!--	<script type="text/javascript" src="js/jquery-1.7.1.min.js"></script>
	<script type="text/javascript" src="js/jquery-ui-1.8.18.custom.min.js"></script>-->
    <!----Revolution slider---->
    <link rel="stylesheet" type="text/css" href="\js\plugin\revolution\css\settings.css">
    <link rel="stylesheet" type="text/css" href="\css\style.min.css">
    @if($lang == 'en')
    <link rel="stylesheet" type="text/css" href="/css/color.css">
    @else
    <link rel="stylesheet" type="text/css" href="/css/arabic.css">
    @endif
    <style>
    html, body{
        overflow-x: hidden;
    }
	#map-canvas{
		width: 320px;
		height: 450px;
	}
</style>

<!-- <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css">
 --><script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyA3Yj4XQ1rNfsgb1heGuRW95ZPLrSs75lo&libraries=places" async defer></script>

    <!-- Favicon Link --> 
    <link rel="shortcut icon" type="image/png" href="/uploads/{{ $settings->favicon}}">
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  <link rel="stylesheet" href="/resources/demos/style.css">
 </head>
<body>
    <!------ Preloader Start ------>
<!--    <div id="loading">-->
<!--  <div id="loading-center">-->
<!--      <img src="\images\loader1.gif" alt="">-->
<!-- </div>-->
<!--</div> -->

   
    <!------ Header Start ------>
    <div class="impl_header_wrapper">
        <div class="impl_logo">
            <a href="/?lang={{ $lang }}"><img src="/uploads/{{ $settings->logo }}" width="200px" alt="Logo" class="img-fluid"></a>
        </div>
        <div class="impl_top_header" >
            <div class="container">
                <div class="row">
                    <div class="col-lg-12 col-md-12">
                        <div class="impl_top_info">
                            <!--@if($lang == 'en')-->
                            <!--<p class="impl_header_time"><i class="fa fa-clock-o" aria-hidden="true"></i> @if(isset($public_texts[0]->value)){{commonController::englishTranslate($public_texts[0]->value)}}@endif</span></p>-->
                            <!--@else-->
                            <!--<p class="impl_header_time"><i class="fa fa-clock-o" aria-hidden="true"></i>@if(isset($public_texts[0]->value)){{commonController::arabicTranslate($public_texts[0]->value)}}@endif</p>-->
                            <!--@endif-->
                            @if($lang == 'en')
                            <ul class="impl_header_icons" style="color: black">
                                <li><button class="btn btn-link" ><a href="?lang=ar"  style="color: black"><i class="fa fa-language" aria-hidden="true"></i> Arabic </a></button></li>
                                @if(Auth::check())
                                <li><form action="/logout?lang={{ $lang }}" method="POST">
                                        {{ csrf_field() }}
                                        <button type="submit" class="btn btn-link" style="color: black"><i class="fa fa-sign-out" aria-hidden="true" style="color: black"></i> Sign Out</button>
                                      </form>
                                  </li>
                                  @else
                                <li><button class="btn btn-link" ><a href="#signin" data-toggle="modal" style="color: black"><i class="fa fa-sign-in" aria-hidden="true" style="color: black"></i> Join Us</a></button></li>
                                @endif
                            </ul>
                            @else
                            <ul class="impl_header_icons" style="color: black">
                                <li><button class="btn btn-link" ><a href="?lang=en"  style="color: black"><i class="fa fa-language" aria-hidden="true"></i> English </a></button></li>
                                @if(Auth::check())
                                <li><form action="/logout" method="POST">
                                        {{ csrf_field() }}
                                        <button type="submit" class="btn btn-link" style="color: black"><i class="fa fa-sign-out" aria-hidden="true" style="color: black"></i> تسجيل خروج</button>
                                      </form>
                                  </li>
                                  @else
                                <li><button class="btn btn-link" ><a href="#signin" data-toggle="modal" style="color: black"><i class="fa fa-sign-in" aria-hidden="true" style="color: black"></i> انضم الينا</a></button></li>
                                @endif
                            </ul>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--menu start-->
        <div class="impl_menu_wrapper">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12 col-md-12">
                        <button class="impl_menu_btn">
                			<i class="fa fa-car" aria-hidden="true"></i>
                			<i class="fa fa-bars" aria-hidden="true"></i>
                		</button>
                        <div class="impl_menu_inner">
                            <div class="impl_logo_responsive">
                                <a href="/"><img src="/uploads/{{ $settings->logo }}" width="200px" alt="Logo" class="img-fluid"></a>
                            </div>
                            @if(Auth::check())
                            <div class="impl_menu">
                                <nav class="hidden-sm hidden-xs">
                                    <div class="menu_cross">
                                        <i class="fa fa-times" aria-hidden="true"></i>
                                    </div>
                                    <ul>
                                        <li style="padding-right: 10px; float: right !important;">
                                        <div class="profile-link" href="#" title="More about {{Auth::user()->first_name}} ">
                                          <img class="img-circle" src="@if(Auth::user()->image){{Auth::user()->image}} @else {{ '/uploads/profile.jpg' }}@endif"/></div>
                                        </li>
                                        <li style="margin-top: 20px"><a href="/?lang={{ $lang }}">@if($lang == 'en'){{'Home'}}@else {{'الصفحة الرئيسية'}} @endif</a></li>
                                        <li style="margin-top: 20px"><a href="#">{{ Auth::user()->first_name }} {{ Auth::user()->last_name }}</a></li>
                                        <li style="margin-top: 20px"><a href="/requests?lang={{$lang}}">@if($lang == 'en'){{'Your Requests'}}@else{{'طلباتك السابقة'}} @endif</a> </li>
                                        <li style="margin-top: 20px"><a href="edit_profile?lang={{ $lang }}">@if($lang == 'en') {{'Edit Profile'}} @else {{ 'تعديل بيانات'}} @endif</a> </li>
                                    </ul>
                                </nav>

                                <nav class="hidden-lg hidden-md">
                                     <div class="menu_cross" style="background-color: transparent !important;">
                                         <i class="fa fa-times" aria-hidden="true"></i>
                                         <!--<h1 style="color: #e01d48; font-family: 'Lobster', cursive !important; border-bottom: 1px #000 double" class="pull-left">{{ $settings->site_name }}</h1>-->
                                    </div>
                                    <ul>
                                        <li style="top: 10px;left: -65px;">
                                            <div class="img-circle" href="#" title="More about {{Auth::user()->first_name}}">
                                              <img class="profile-pic" src="@if(Auth::user()->image){{Auth::user()->image}} @else {{ '/uploads/profile.jpg' }}@endif" style="width:70%"/>
                                            </div>
                                        </li>
                                        <li style="margin-top: 20px;"><a href="#" style="@if($lang=='en'){{"top: -60px; left: 105px; font-size: 13px;color: black;"}}@else {{"top: -60px;left: 48px;font-size: 13px;color: black;"}} @endif">{{ Auth::user()->first_name }} {{ Auth::user()->last_name }}</a></li>
                                        
                                        <li style="margin-top: 20px"><a href="/?lang={{ $lang }}">@if($lang == 'en'){{'Home'}}@else {{'الصفحة الرئيسية'}} @endif</a></li>
                                        
                                        <li style="margin-top: 20px"><a href="/requests?lang={{$lang}}">@if($lang == 'en'){{'Your Requests'}}@else{{'طلباتك السابقة'}} @endif</a> </li>

                                 @if($lang == 'en')  
                                        <li style="margin-top: 20px; "><a href="edit_profile?lang={{ $lang }}" >Edit Profile</a> </li>
                                        <li style="margin-top: 20px; " ><a href="/book_now?lang={{ $lang }}">Book Now</a> </li>

                                        @else
                                         <li style="margin-top: 20px;" ><a href="edit_profile?lang={{ $lang }}">تعديل بيانات</a> </li>
                                        <li style="margin-top: 20px; "><a href="/book_now?lang={{ $lang }}">احجز الان</a> </li>
                                        @endif
                                    </ul>
                                </nav>
                            </div>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Form -->
    @if($lang == 'en')
    <!------ Sell wrapper  Start ------>
    <div class="impl_sell_wrapper one" style="height: 1250px !important">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12" style="text-align: center">
					<h1 >Book Now</h1>
                </div>
                <div class="col-lg-12 col-md-12">
                   <div id="overlay"></div>
	<form action="/order_now" class="container booking" name="booking" method="POST" id="myform">
	{{ csrf_field() }}
	<div class="persons">
			         <input type="hidden" name="lang" value="{{ $lang }}">

				<label>Services</label>
				<div class="input-text">
					<select name="service_id" id="service_id">
                        <option disabled selected>-Choose Service-</option>
						@foreach($services as $service)
                            <option value="{{ $service->id }}">{{ $service->name }}</option>
                        @endforeach   
					</select>
					<div class="icon"></div>
				</div>
			
		</div>
		
			<div class="dates" data-type="none" style="display: none" id="priceDisplay">
			<label for="checkin">Price Of Service</label>
			<div class="input-text" id="price" style="text-align: center"> 
			</div>
		</div>
		
        <div class="form-group">
            <label>Date:</label>

            <div class="input-group date" style="direction: rtl;">
              <div class="input-group-addon" style="background-color: #404a57;color: white;">
                <i class="fa fa-calendar"></i>
              </div>
                <input type="text" id="datepicker" onchange="handler(event);" name="date" autocomplete="off" min="{{ $date }}" style="width: 280px;height: 44px;text-align: left;font-size: 18px;padding-left: 8px;">
            </div>
            <!-- /.input group -->
          </div>
		<div class="persons">
			
				<label>Choose Service Timing</label>
				<div class="input-text">
                    <select name="time" id="date_rest_time">          					
                    </select>
					<div class="icon"></div>
				</div>
			
		</div>
		<br>
		 <div class="form-group">
				<label for="">Choose Location</label>
				<input type="text" id="searchmap">
				<br>
				<div id="map-canvas"></div>
			</div>

			<div class="form-group">
				<input type="hidden" class="form-control input-sm" name="lat" id="lat">
			</div>

			<div class="form-group">
				<input type="hidden" class="form-control input-sm" name="long" id="lng">
			</div>

        @if(count($errors))
          @foreach($errors->all() as $error)
              <script type="text/javascript">
                  alert('{{ $error }}');
              </script>
          @endforeach
        @endif
		 <div class="form-group">
            <center>
                <button class="impl_btn" id="form-button-submit"> BOOK NOW</button>
            </center>
         </div>
	</form>
                </div>
            </div>
        </div>
    </div>
 @else
 <div class="impl_sell_wrapper one" style="height: 1250px !important" >
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12" style="text-align: center">
					<h1 >احجز الأن</h1>
                </div>
                <div class="col-lg-12 col-md-12">
                   <div id="overlay"></div>
<form action="/order_now" class="container booking" name="booking" method="POST"  id="myform">
	{{ csrf_field() }}	
	<div class="persons">
			<input type="hidden" name="lang" value="{{ $lang }}">
				<label>الخدمات</label>
				<div class="input-text">
					<select name="service_id" id="service_id">
                        <option disabled selected>-اختر  الخدمة-</option>
						@foreach($services as $service)
                            <option value="{{ $service->id }}">{{ $service->name }}</option>
                        @endforeach   
					</select>
					<div class="icon"></div>
				</div>
			
		</div>
		
			<div class="dates" data-type="none" style="display: none" id="priceDisplay">
			<label for="checkin">السعر</label>
			<div class="input-text" id="price" style="text-align: center">
			</div>
		</div>
		
		
		<div class="form-group">
            <label>التاريخ:</label>

            <div class="input-group date" style="direction: rtl;">
              <div class="input-group-addon" style="background-color: #404a57;color: white;">
                <i class="fa fa-calendar"></i>
              </div>
                <input type="text" id="datepicker" onchange="handler(event);" name="date" autocomplete="off" min="{{ $date }}" style="width: 280px;height: 44px;text-align: left;font-size: 18px;padding-left: 8px;">
            </div>
            <!-- /.input group -->
          </div>
		
		<div class="persons">
			
				<label>اختار وقت الخدمة</label>
				<div class="input-text">
				<select name="time" id="date_rest_time">                            
				</select>
					<div class="icon"></div>
				</div>
			
		</div>
		<br>
		 <div class="form-group">
				<label for="">حدد موقعك</label>
				<input type="text" id="searchmap">
				<div id="map-canvas"></div>
			</div>

			<div class="form-group">
				<input type="hidden" class="form-control input-sm" name="lat" id="lat">
			</div>

			<div class="form-group">
				<input type="hidden" class="form-control input-sm" name="long" id="lng">
			</div>


		 <div class="form-group">
            <center>
                <button class="impl_btn" id="form-button-submit"> احجز الأن</button>
            </center>
         </div>
        @if(count($errors))
          @foreach($errors->all() as $error)
              <script type="text/javascript">
                  alert('{{ $error }}');
              </script>
          @endforeach
        @endif
	</form>
                </div>
            </div>
        </div>
    </div>
 @endif
 <div class="impl_footer_wrapper">
        <div class="impl_social_wrapper">
            <ul>
                <li><a href="{{$settings->fb_link}}"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                <li><a href="{{$settings->twitter_link}}"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                <li><a href="{{$settings->google_plus}}"><i class="fa fa-google-plus" aria-hidden="true"></i></a></li>
                <li><a href="{{$settings->youtube_link}}"><i class="fa fa-youtube-play" aria-hidden="true"></i></a></li>
                <li><a href="{{$settings->linkedin_link}}"><i class="fa fa-rss" aria-hidden="true"></i></a></li>
                <li><a href="{{$settings->linkedin_link}}"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
                <li><a href="{{$settings->fb_link}}"><i class="fa fa-pinterest-p" aria-hidden="true"></i></a></li>
                <li><a href="{{$settings->instgram_link}}"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
            </ul>
        </div>
    </div>
    <!----Bottom Footer Start---->
    <div class="impl_btm_footer">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12">
                    <p class="impl_copyright" style="color: white">Copyright &copy; {{ date('Y') }} {{ strtoupper($settings->site_name) }}. All Rights Reserved to <a href="http://gtsaw.com" style="color: #e01b49">GTSAW</a></p>
                </div>
            </div>
        </div>      
    </div> 
//     <script>
//         $( document ).ready(function() {
  
//   $('#form-button-submit').click(function(){
//     $('#myform').submit();
//   });
    
// });
//     </script>
    <script>
	var map = new google.maps.Map(document.getElementById('map-canvas'),{
		center:{
			lat: 30.00862208596517,
        	lng: 31.428153915405346
		},
		zoom:14,
		mapTypeId: 'roadmap'

	});

    var image = 'https://developers.google.com/maps/documentation/javascript/examples/full/images/beachflag.png';
	
    var infoWindow = new google.maps.InfoWindow({map: map});

    var marker = new google.maps.Marker({
        position: {
            lat: 30.00862208596517,
            lng: 31.428153915405346

        },
        map: map,
        // icon: image,
        draggable: true
    });
    

    // var lat = 30.00862208596517;
    // var lng =  31.428153915405346;
    // $('#lat').val(lat);
    // $('#lng').val(lng);


//Try HTML5 geolocation.
if (navigator.geolocation) {
    navigator.geolocation.getCurrentPosition(function(position) {
        var pos = {
            lat: position.coords.latitude,
            lng: position.coords.longitude
        };
         marker.setPosition(pos);

        google.maps.event.addListener(marker,'position_changed',function(){
        var latt = marker.getPosition().lat();
        var lngt = marker.getPosition().lng();
        $('#lat').val(latt);
        $('#lng').val(lngt);
        });
        map.setCenter(pos);
    }, function() {
        handleLocationError(true, map.getCenter());
    });
} else {
    // Browser doesn't support Geolocation
    // alert('');
    handleLocationError(false, map.getCenter());
}

function handleLocationError(browserHasGeolocation, pos) {
         
    // infoWindow.setPosition(pos);
    // infoWindow.setContent(browserHasGeolocation ?
    //                       'Error: The Geolocation service failed.' :
                          // 'Error: Your browser doesn\'t support geolocation.');
}
    google.maps.event.addListener(marker,'position_changed',function(){
        var lat = marker.getPosition().lat();
        var lng = marker.getPosition().lng();
        $('#lat').val(lat);
        $('#lng').val(lng);
    });
     map.addListener('center_changed', function() {
    // 3 seconds after the center of the map has changed, pan back to the
    // marker.
    window.setTimeout(function() {
      map.panTo(marker.getPosition());
    }, 10000);
      });
      
    google.maps.event.addListener(map, 'click', function(e) {
    placeMarker(e.latLng, map);
  });

  function placeMarker(position, map) {
    marker.setPosition(position);
  }
  
	var searchBox = new google.maps.places.SearchBox(document.getElementById('searchmap'));
	google.maps.event.addListener(searchBox,'places_changed',function(){
		var places = searchBox.getPlaces();
		var bounds = new google.maps.LatLngBounds();
		var i, place;
		for(i=0; place=places[i];i++){
  			bounds.extend(place.geometry.location);
  			marker.setPosition(place.geometry.location); //set marker position new...
  		}
  		map.fitBounds(bounds);
  		map.setZoom(15);
	});
</script> 
<script>
    $('#searchmap').on('keypress', function(e) {
    return e.which !== 13;
});
</script>
   <script type="text/javascript">
         $("#service_id").on('change',function(){
              var id = $("#service_id").val();
              $.ajax({
              type: "GET",
              url: '/service/' + id,
              data: {},
              dataType: "json",
              success: function( msg ) {
                $("#priceDisplay").show();
                $("#price").html(msg.message);
              }
              
          });

            });
    </script>
    <!---- Go To Top---->
    <!--<span class="gotop"><img src="\images\car.png" alt=""></span>-->
    <!--Main js file Style-->
 
  <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.5/jquery.min.js"></script>  
   <script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8/jquery-ui.min.js"></script> 

    <script type="text/javascript" src="\js\jquery.js"></script>
    <script type="text/javascript" src="\js\custom.js"></script>
    <script type="text/javascript" src="\js\calendar.js"></script>
    <script type="text/javascript" src="\js\popper.js"></script>
    <script type="text/javascript" src="\js\bootstrap.min.js"></script>
    <script type="text/javascript" src="\js\ion.rangeSlider.min.js"></script>
    <script type="text/javascript" src="\js\plugin\magnific\jquery.magnific-popup.min.js"></script>
    <script type="text/javascript" src="\js\plugin\slick\slick.min.js"></script>
    <script type="text/javascript" src="\js\plugin\nice_select\jquery.nice-select.min.js"></script>
    <!----------Revolution slider start---------->
    <script type="text/javascript" src="\js\plugin\revolution\js\jquery.themepunch.revolution.min.js"></script>
    <script type="text/javascript" src="\js\plugin\revolution\js\jquery.themepunch.tools.min.js"></script>
    <script type="text/javascript" src="\js\plugin\revolution\js\revolution.extension.kenburn.min.js"></script>
    <script type="text/javascript" src="\js\plugin\revolution\js\revolution.extension.layeranimation.min.js"></script>
    <script type="text/javascript" src="\js\plugin\revolution\js\revolution.extension.navigation.min.js"></script>
    <script type="text/javascript" src="\js\plugin\revolution\js\revolution.extension.parallax.min.js"></script>
    <script type="text/javascript" src="\js\plugin\revolution\js\revolution.extension.slideanims.min.js"></script>
    <script type="text/javascript" src="\js\plugin\revolution\js\revolution.extension.actions.min.js"></script>
    <script type="text/javascript" src="\js\plugin\revolution\js\revolution.addon.slicey.min.js"></script>
    <!----------Revolution slider start---------->
    <script type="text/javascript" src="\js\custom.min.js"></script>
    <!--<script type="text/javascript" src="\js\calendar.js"></script>-->
    <!--Main js file End-->
 
    <script>
    
    $(document).ready(function(){
 $('.pop-up').on('click', function(){
    $('#overlay').fadeIn(300); 
    $('.calendar').fadeIn(300); 
    let clickedbutton = $("input",$(this).parent()).attr('id');
    $('.dates').data('type',clickedbutton);
 });
 
 $('table').on('click', function(event){
   let that=$(event.target);
    if(that.is('td') && !that.hasClass('notCurMonth') && !that.hasClass('holiday') && !that.hasClass('curDay')){
        $('td.curDay').toggleClass('curDay');
        that.toggleClass('curDay');
    }
}); 

$('#add_event').on('click', function(){
    let value= $('td.curDay').html();
    $('#overlay').fadeOut(300);
    $('.calendar').fadeOut(300);
    let id=($('.dates').data()).type;
    $('#' + id).val(value+" {{date('M, Y')}}");
}); 

$('#search').on('click', function(e){
    $('.booking').addClass('is-sent');
    e.preventDefault();
});
}); 


    </script>
    


</body>

</html>