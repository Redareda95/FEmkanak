@extends('master')
@section('content')
@php
  $lang = (isset($_GET['lang'])) ? $_GET['lang'] : 'en' ;
@endphp 
  
    <!------ Contact Wrapper Start ------>
@if($flash = session('message'))
<div class="alert success" style="text-align: center;color: green;font-size: xx-large;">
  <span class="closebtn" onclick="this.parentElement.style.display='none';">&times;</span> 
    <b>{{ $flash }}</b>
</div>
@endif
@if($lang == 'en')
    <div class="impl_contact_wrapper one">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12" style="text-align: center">
					<h1 >Edit Profile</h1>
                </div>
    
                <div class="col-lg-12 col-md-12">


                        <form action="{{url('/edit_profile_action')}}" method="post"class="container booking" name="booking"  enctype="multipart/form-data">

                                {!!csrf_field()!!}
                                @if ($errors->any())
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                                @endif
                                {{-- <div class="preview img-wrapper">

                                </div> --}}

            		<div class="dates" data-type="none">
			<label for="checkin">First Name</label>
			<div class="input-text">
                            <input type="text" value="{{$user->first_name}}"name="first_name">
			</div>
		</div>
			<div class="dates" data-type="none">
			<label for="checkin">Last Name</label>
			<div class="input-text" >
			<input type="text" name="last_name" value="{{$user->last_name}}">
			</div>
		</div>
			<div class="dates" data-type="none">
			<label for="checkin">Phone</label>
			<div class="input-text">
                            <input type="tel" value="{{$user->phone}}" name="phone">
			</div>
		</div>
			<div class="dates" data-type="none" >
			<label for="checkin">Email</label>
			<div class="input-text">
                            <input type="text" value="{{$user->email}}" name="email">
			</div>
		</div>
                                <div class="file-upload-wrapper">
                                    <div class="">
                                        <img id="img_profile" src="@if($user->image){{url($user->image)}}@else{{url('uploads/profile.jpg')}}@endif" style="width: 100%; height: 220px;">
                                    </div>
                                    <div class="btn" >
                <button type="button" class="btn btn-file" style="width: 31%;color: transparent;"><input type="file" name="file" id="profile_image"accept="image/*" /></button>
                <button type="button" class="btn btn-default" style="width: 31%;height: 38px;" onclick="reset_image()">Cancel</button>
                </div>
               </div>

		 <div class="impl_pst_img_icon" style="top:100%"> <button class="impl_btn"> Save Edit </button> </div>
	</form>
                </div>
            </div>
        </div>
    </div>
@else
    <div class="impl_contact_wrapper one">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12" style="text-align: center">
                    <h1>تعديل بيانات شخصية</h1>
                </div>
    
                <div class="col-lg-12 col-md-12">


                        <form action="{{url('/edit_profile_action')}}" method="post"class="container booking" name="booking"  enctype="multipart/form-data">

                                {!!csrf_field()!!}
                                @if ($errors->any())
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                                @endif
                                {{-- <div class="preview img-wrapper">

                                </div> --}}

                    <div class="dates" data-type="none">
            <label for="checkin">الاسم الاول</label>
            <div class="input-text">
                <input type="text" value="{{$user->first_name}}"name="first_name">
            </div>
        </div>
            <div class="dates" data-type="none">
            <label for="checkin">الاسم الأخير</label>
            <div class="input-text" >
            <input type="text" name="last_name" value="{{$user->last_name}}">
            </div>
        </div>
            <div class="dates" data-type="none">
            <label for="checkin">رقم الموبايل</label>
            <div class="input-text">
                            <input type="tel" value="{{$user->phone}}" name="phone">
            </div>
        </div>
            <div class="dates" data-type="none" >
            <label for="checkin">اليميل</label>
            <div class="input-text">
                            <input type="text" value="{{$user->email}}" name="email">
            </div>
        </div>
        <div class="file-upload-wrapper">
            <div class="">
                <img id="img_profile" src="@if($user->image){{url($user->image)}}@else{{url('uploads/profile.jpg')}}@endif" style="width: 100%; height: 220px;">
            </div>
            <div class="btn" >
                <button type="button" class="btn btn-file" style="width: 31%;color: transparent;"><input type="file" name="file" id="profile_image"accept="image/*" /></button>
                <button type="button" class="btn btn-default" style="width: 31%;height: 38px;" onclick="reset_image()">الغاء</button>
                </div>
               </div>

         <div class="impl_pst_img_icon" style="top:100%"> <button class="impl_btn">  حفظ التغييرات </button> </div>
    </form>
                </div>
            </div>
        </div>
    </div>
@endif
@stop