@extends('master')
@section('content')
@php
  $lang = (isset($_GET['lang'])) ? $_GET['lang'] : 'en' ;
@endphp 
<?php 
use App\Http\Controllers\commonController;
?>
<style>
            
body{
    background:#000;
}
.login-wrap{
    width:100%;
    margin:auto;
    max-width:525px;
    min-height:830px;
    position:relative;
    background:url(images/22.jpg) no-repeat center;
    box-shadow:0 12px 15px 0 rgba(225,0,0,.24),0 17px 50px 0 rgba(225,0,0,.19);
}
strong{
    color: white;
}

    </style>
 <div class="impl_contact_wrapper ">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12" style="text-align: center; padding-bottom:20px">
                    <h1 >@if($lang =='en'){{'Registration'}} @else {{ 'أنضم ألينا' }} @endif</h1>
                </div>
                <div class="col-lg-12 col-md-12">
<div class="login-wrap">
    <div class="login-html">
        <input id="tab-1" type="radio" name="tab" class="sign-in" checked><label for="tab-1" class="tab">@if($lang =='en') {{'Sign In'}} @else {{ 'سجل دخول' }} @endif</label>
        <input id="tab-2" type="radio" name="tab" class="sign-up"><label for="tab-2" class="tab">@if($lang =='en') {{'Register'}} @else {{'انشاء حساب'}} @endif</label>
    
        <div class="login-form">
            <div class="sign-in-htm" style="position:relative;top: 131px;">
                <form method="POST" action="{{ route('login') }}" aria-label="{{ __('Login') }}">
                      @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                <li>{!! $error !!}</li>
                                @endforeach
                            </ul>
                        </div>
                        @endif
                    @csrf
                <div class="group">
                    <label for="user" class="label">@if($lang =='en') {{'Email'}} @else {{' البريد الالكتروني'}} @endif</label>
                    <input id="user" name="email" type="text" class="input">
                    @if ($errors->has('email'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('email') }}</strong>
                            </span>
                    @endif
                </div>
                <div class="group">
                    <label for="pass" class="label">@if($lang =='en'){{'Password'}}@else {{'كلمة المرور'}}@endif</label>
                    <input id="pass" type="password" class="input" data-type="password" name="password" required>
                    @if ($errors->has('password'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('password') }}</strong>
                            </span>
                    @endif
                </div> 
                <div class="group">
                    <input id="check" type="checkbox" type="checkbox" name="remember" class="check" {{ old('remember') ? 'checked' : '' }} style="color: white">
                    <label for="check" style="color: white"><span class="icon"></span>@if($lang =='en') {{"Keep me Signed in"}} @else {{'  البقاء قيد التسجيل '}} @endif</label>
                </div>
                    <div class="group" style="margin-left:100px">
                  <button type="submit" class="impl_btn submitForm">@if($lang =='en'){{'Login'}} @else {{' سجل دخول '}}@endif</button>
                </form>
                </div>
                <div class="hr"></div>
                <div class="foot-lnk">
                    <a href="{{ route('password.request') }}?lang={{ $lang }}" style=font-weight:bolder>@if($lang =='en'){{'Forgot Password?'}}@else {{'نسيت كلمة المرور؟'}}@endif</a>
                </div>
            </div>
            <div class="sign-up-htm">
                <form method="POST" action="{{ route('register') }}" aria-label="{{ __('Register') }}">
                      @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                        @endif
                                            @csrf
                <div class="group">
                    <label for="user" class="label">@if($lang =='en') {{'First Name'}} @else {{'الاسم الاول '}}@endif </label>
                    <input id="user" type="text" class="input" name="first_name">
                    @if ($errors->has('first_name'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('first_name') }}</strong>
                        </span>
                    @endif
                </div>
                <div class="group">
                    <label for="user" class="label">@if($lang =='en'){{"Last Name"}}@else{{'الاسم الاخير'}} @endif</label>
                    <input id="user" type="text" class="input" name="last_name">
                     @if ($errors->has('last_name'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('last_name') }}</strong>
                        </span>
                    @endif
                </div>
                 <div class="group">
                    <label for="pass" class="label">@if($lang =='en'){{"Email Address"}}@else{{"البريد الألكتروني"}}@endif</label>
                    <input id="pass" type="text" class="input" name="email">
                </div>
                @if ($errors->has('email'))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('email') }}</strong>
                    </span>
                @endif
                <div class="group">
                    <label for="pass" class="label">@if($lang =='en'){{"Phone"}}@else {{"رقم الهاتف "}} @endif</label>
                    <input id="pass" type="text" class="input" name="phone">
                </div>
                 @if ($errors->has('phone'))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('phone') }}</strong>
                    </span>
                @endif
                <div class="group">
                    <label for="pass" class="label">@if($lang =='en'){{"Password"}}@else {{"كلمة المرور"}}@endif</label>
                    <input id="pass" type="password" class="input" data-type="password" name="password">
                     @if ($errors->has('password'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('password') }}</strong>
                        </span>
                    @endif
                </div>
                <div class="group">
                    <label for="pass" class="label">@if($lang =='en'){{"Repeat Password"}}@else{{"تأكيد كلمة المرور"}} @endif</label>
                    <input id="pass" type="password" class="input" data-type="password" name="password_confirmation">
                </div>
                <div class="group" style="margin-left:100px">
                  <button type="submit" class="impl_btn submitForm">@if($lang =='en'){{"Register"}}@else{{"انشاء حساب"}} @endif</button>
                </div>
                </form>
                <div class="hr"></div>
            </div>
        </div>
    </div>
</div>
</div>
</div>
</div>
</div>   
@stop