@extends('master')
@section('content')
@php
  $lang = (isset($_GET['lang'])) ? $_GET['lang'] : 'en' ;
@endphp 
<?php 
use App\Http\Controllers\commonController;
?>
 <style>
            
body{
        background:#000;
}
.login-wrap{
    width:100%;
    margin:auto;
    max-width:525px;
    min-height:380px;
    position:relative;
    background:url(/images/22.jpg) no-repeat center;
    box-shadow:0 12px 15px 0 rgba(225,0,0,.24),0 17px 50px 0 rgba(225,0,0,.19);
}

    </style>
<!------ Contact Wrapper Start ------>
    <div class="impl_contact_wrapper ">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12" style="text-align: center; padding-bottom:20px">
                    <h1 >@if($lang == 'en'){{'Forget Password'}}@else {{ 'نسيت كلمة المرور' }} @endif</h1>
                </div>
                 @if (session('status'))
                    <div class="alert alert-success" role="alert">
                        {{ session('status') }}
                    </div>
                @endif
<div class="col-lg-12 col-md-12">
<div class="login-wrap">
    <div class="login-html">
     <form method="POST" action="{{ route('password.email') }}" aria-label="{{ __('Reset Password') }}">
                        @csrf
        <div class="login-form" style="margin-top: 50px">
                <div class="group">
                    <label for="user" class="label">@if($lang == 'en') {{'Email'}} @else {{"البريد الالكتروني"}} @endif</label>
                    <input id="user" type="email" class="input" name="email" value="{{ old('email') }}" required>
                </div>
                    <div class="group" style="margin-left:100px">
                 <center>
                  <button type="submit" class="impl_btn submitForm">@if($lang == 'en'){{ __('Send Password Reset Link') }}@else {{"ارسل رابط تغيير كلمة المرور "}}  @endif</button>
                  </center>
                </div>
            </form>      
        </div>
    </div>
</div>
            </div>
 </div>
            </div>
    </div>
@endsection

